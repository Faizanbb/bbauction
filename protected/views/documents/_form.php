<?php
/* @var $this DocumentsController */
/* @var $model Documents */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'documents-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'method' => 'post'
	)
)); ?>

	<?php echo $form->errorSummary($model); ?>
	<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type}' style='color:red;'>{$flash}</div>";
		}
	?>
	<br><br>
<!-- <input name="_token" type="hidden" value="{{ csrf_token() }}"/> -->
	<div class="row">
		<?php echo $form->checkbox($model,'incinvite'); ?>
		<?php echo $form->labelEx($model,'include in invite', array('for'=>'Documents_incinvite')); ?>
		<?php echo $form->error($model,'incinvite'); ?>
	</div>

	<br><br>

	<!-- Dropzone -->
	<div class="row dropzone" id="my-dropzone"></div>

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'user id'); ?>
		<?php //echo $form->textField($model,'userid'); ?>
		<?php //echo $form->error($model,'userid'); ?>
	</div> -->

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'event id'); ?>
		<?php //echo $form->textField($model,'event_id'); ?>
		<?php //echo $form->error($model,'event_id'); ?>
	</div> -->

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'document name'); ?>
		<?php //echo $form->textField($model,'doc_name',array('size'=>60,'maxlength'=>350)); ?>
		<?php //echo $form->error($model,'doc_name'); ?>
	</div> -->
	

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'file location'); ?>
		<?php //echo $form->fileField($model,'file_loc',array('size'=>60,'maxlength'=>350)); ?>
		<?php //echo $form->error($model,'file_loc'); ?>
	</div> -->


	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'createdat'); ?>
		<?php //echo $form->textField($model,'createdat'); ?>
		<?php //echo $form->error($model,'createdat'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit-all')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->



