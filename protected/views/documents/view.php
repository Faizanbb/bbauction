<?php
/* @var $this DocumentsController */
/* @var $model Documents */

$this->breadcrumbs=array(
	'Documents'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Documents', 'url'=>array('index')),
	array('label'=>'Create Documents', 'url'=>array('create')),
	array('label'=>'Update Documents', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Documents', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Documents', 'url'=>array('admin')),
);
?>

<h1>View Documents #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'userid',
		'event_id',
		'doc_name',
		'file_loc',
		'incinvite',
		'createdat',
	),
)); ?>

<?php // $this->widget('ext.dropzone.EDropzone', array(
//     'model' => $model,
//     'attribute' => 'file',
//     'url' => $this->createUrl('controller/action'),
//     'mimeTypes' => array('image/jpeg', 'image/png'),
//     'onSuccess' => 'someJsFunction();',
//     'options' => array(),
// )); ?>

<!-- Display pdf files through this code
<div> <object data="test.pdf" type="application/pdf" width="300" height="200">
alt : <a href="test.pdf">test.pdf</a>
</object>
</div> -->