<?php
/* @var $this EventsController */
/* @var $model Events */
/* @var $auction Auction */
/* @var $rfq Rfq */
/* @var $form CActiveForm */
?>

<div class="form row">
    <div class='col-sm-8'>
        
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'events-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	
	<?php if($form->errorSummary($model)||$form->errorSummary($auction)||$form->errorSummary($rfq)){?>
<div class="alert alert-danger">
  <strong>Danger!</strong> 
      <?php 
        echo $form->errorSummary($model);
        echo $form->errorSummary($auction);
        echo $form->errorSummary($rfq);
//        echo '<pre>';
//        print_r($model);
//        print_r($auction);exit;
        ?>
</div>
        <?php } ?>
	
        <?php
        if($model->scenario=='update'){
            
        ?>
	<div class="form-group">
		<?php echo $form->textField($model,'ID',array('value'=>$model->ID,'class'=>'nodisplay')); ?>
	</div>
        <?php } ?>
        <div class="form-group">
		<?php echo $form->labelEx($model,'event_title'); ?>
		<?php echo $form->textField($model,'event_title',array('size'=>60,'maxlength'=>350 ,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'event_title'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'event_description'); ?>
		<?php echo $form->textArea($model,'event_description',array('rows'=>6, 'cols'=>50,'class'=>'wysiwygEditor form-control')); ?>
		<?php echo $form->error($model,'event_description'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'seal_result'); ?>
		<?php echo $form->checkBox($model,'seal_result'); ?>
		<?php echo $form->error($model,'seal_result'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'multi_currency'); ?>
		<?php echo $form->checkBox($model,'multi_currency',array('class'=>'form-check-input')); ?>
		<?php echo $form->error($model,'multi_currency'); ?>
	</div>
        <div class="form-group">
            
        	<?php echo $form->labelEx($model,'questionnaire '); ?>
		<?php echo $form->checkBox($model,'questionnaire',array('onchange'=>'changeAddQuestionaire(this.id)')); ?>
		<?php echo $form->error($model,'questionnaire'); ?>
	</div>
        <div id="questionaire_fields" class='nodisplay'> 
            <?php 
            if(!isset($questionaire)&&empty($questionaire)){
             
            ?>
            <div class="Questionaire">
                <h3>Questionaire # 1</h3>
                <input type="hidden" name="Questionaire[0][ID]" />
                  <div class="form-group">
                    <label >What do you want to call it ?</label>
                    <input  class="form-control questionaire-title" id="Questionaire_title_0" onchange="changeQuestionairetitle()" name="Questionaire[0][title]">
                    <small id="emailHelp" class="form-text text-muted">
                    (e.g. RFI, RFP, RFQ, PQQ, Supplier Self certification, Survey)
                    </small>
                  </div>
                  <div class="form-group">
                    <label >Deadline</label>
                    <input  class="form-control datetimepicker" name="Questionaire[0][deadline]">
                  </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" name="Questionaire[0][with_prequalification]"  >
                    <label class="form-check-label" >
                      Do you want it to be pre-qualified?
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="Questionaire[0][with_scoring]" id="questionaire_with_scoring_0" onchange='changeQuestionaireWithScoring(this.id,0)' >
                    <label class="form-check-label" >
                      Do you want it to have scoring
                    </label>
                </div>
                <div class="form-group nodisplay " id='question_with_weight_0'>
                    <label >Do you want it to have weighting</label>
                    <select class="form-control" name='Questionaire[0][with_weight]' id='questionaire_with_weight_0'  >
                      <option value='0'>No</option>
                      <option value='1'>Per Section</option>
                      <option value='2'>Per Question</option>
                      <option value='3'>Both Per Section & Per Question</option>
                    </select>
                </div>
            </div>
            
            <div id='extra_questionaire'>
                
            </div>
               
            <?php }else { 
                $count = 0;
                
                foreach($questionaire as $qustnair){
              
                ?>
                <div class="Questionaire">
                    
                    <h3>Questionaire # <?php $quesno =  $count+1; echo $quesno; ?></h3>
                    <?php  ?>
                    <input type="hidden" name="Questionaire[<?php echo $qustnair['ID'];?>][ID]" value="<?php echo $qustnair['ID'];?>"/>
                      <div class="form-group">
                        <label >What do you want to call it ?</label>
                        <input value="<?php echo $qustnair['title'];?>"  class="form-control questionaire-title" id="Questionaire_title_0" onchange="changeQuestionairetitle()" name="Questionaire[<?php echo $qustnair['ID'];?>][title]">
                        <small id="emailHelp" class="form-text text-muted">
                        (e.g. RFI, RFP, RFQ, PQQ, Supplier Self certification, Survey)
                        </small>
                      </div>
                    <div class="form-group">
                        <label >Deadline</label>
                        <input  class="form-control datetimepicker" value="<?php echo $qustnair['deadline'];?>" name="Questionaire[<?php echo $qustnair['ID'];?>][deadline]">
                      </div>
                   
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox"  <?php if($qustnair['pre_qualify']==1) echo 'checked="checked"'; ?> name="Questionaire[<?php echo $qustnair['ID'];?>][with_prequalification]"  >
                        <label class="form-check-label" >
                          Do you want it to be pre-qualified?
                        </label>
                    </div>
                    
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="Questionaire[<?php echo $qustnair['ID'];?>][with_scoring]" id="questionaire_with_scoring_0"  <?php if($qustnair['scoring']==1) echo 'checked="checked"'; ?> onchange='changeQuestionaireWithScoring(this.id,0)' >
                        <label class="form-check-label" >
                          Do you want it to have scoring
                        </label>
                    </div>
                    <div class="form-group nodisplay " value="<?php echo $qustnair['weighting'];?>" id='question_with_weight_0'>
                        <label >Do you want it to have weighting</label>
                        <select class="form-control" name='Questionaire[<?php echo $qustnair['ID'];?>][with_weight]' id='questionaire_with_weight_0'  >
                          <option value='0'>No</option>
                          <option value='1'>Per Section</option>
                          <option value='2'>Per Question</option>
                          <option value='3'>Both Per Section & Per Question</option>
                        </select>
                    </div>
                </div>
            
            <?php $count++; }
                 } ?>
            <div id='extra_questionaire'>
                
            </div>
            
            <div class='form-group'>
                <input id='addextQuestion' type="checkbox" class='form-check-input' onchange="addNewQuestionaier('addextQuestion')"  />
                <label>Add another Questionaire</label>
            </div>
        </div>
	<div class="form-group">
		
                <?php
                    $radionbtns = array('1'=>'RFQ', '2'=>'Auction');
                echo $form->radioButtonList($model,'event_type',
                        $radionbtns,
                        array(
                            'template'=>'<div>{label}</div> <div>{input}</div> ',
                            'class'=>'hello')
                        );
                ?>
		<?php echo $form->error($model,'event_type'); ?>
	</div>
	

	

        <div id="auction" class="nodisplay">
            <h2>Auction</h2>
            <div class="form-group" >
                    <?php echo $form->labelEx($auction,'starttime'); ?>
                    <?php echo $form->textField($auction,'starttime',array('class'=>'datetimepicker form-control')); ?>
                    <?php echo $form->error($auction,'starttime'); ?>
            </div>
            <div class="form-group" >
                    <?php echo $form->labelEx($auction,'direction'); ?>
                    <?php echo $form->dropDownList($auction,'direction',array('Reverse'=>'Reverse','Forward'=>'Forward'),array('class'=>'form-control')); ?>
                    <?php echo $form->error($auction,'direction'); ?>
            </div>
            <div class="form-group" >
                    <?php echo $form->labelEx($auction,'auction'); ?>
                    <?php echo $form->dropDownList($auction,'auction',array('Ranked'=>'Ranked','Open'=>'Open','Japanese'=>'Japanese'),array('class'=>'form-control')); ?>
                    <?php echo $form->error($auction,'auction'); ?>
            </div>
            <div class="form-group" >
                    <?php echo $form->labelEx($auction,'minduration'); ?>
                    <?php echo $form->dropDownList($auction,'minduration',array(0=>'None',60=>'Last Minute',120=>'Last Two Minutes',300=>'Last 5 Minutes',600=>'Last 10 Minutes',900=>'Last 15 Minutes'),array('class'=>'form-control')); ?>
                    <?php echo $form->error($auction,'minduration'); ?>
            </div>
            <div class="form-group" >
                    <?php echo $form->labelEx($auction,'dynamic_close_period'); ?>
                    <?php echo $form->dropDownList($auction,'dynamic_close_period',array(0=>'None',60=>'Last Minute',120=>'Last Two Minutes',300=>'Last 5 Minutes',600=>'Last 10 Minutes',900=>'Last 15 Minutes'),array('onchange'=>'changeDynamicClosePeriod(this.value)','class'=>'form-control')); ?>
                    
                    <?php echo $form->error($auction,'dynamic_close_period'); ?>
            </div>
            <div class="form-group nodisplay applies_to">
                    <?php echo $form->labelEx($auction,'applies_to'); ?>
                    <?php echo $form->dropDownList($auction,'applies_to',array(1=>'Top 1 bidder',2=>'Top 2 bidders',3=>'Top 3 bidders',4=>'Top 4 bidders',5=>'Top 5 bidders'),array('class'=>'form-control')); ?>
                    <?php echo $form->error($auction,'applies_to'); ?>
            </div>
            <div class="form-group">
                    <?php echo $form->labelEx($auction,'min_bid_changes'); ?>
                    <?php echo $form->textField($auction,'min_bid_changes',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
                    <?php echo $form->error($auction,'min_bid_changes'); ?>
            </div>
            <div class="form-group">
                    <?php echo $form->labelEx($auction,'max_bid_change'); ?>
                    <?php echo $form->textField($auction,'max_bid_change',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
                    <?php echo $form->error($auction,'max_bid_change'); ?>
            </div>
            <div class="form-group">
                    <?php echo $form->labelEx($auction,'weighting'); ?>
                     <?php echo $form->dropDownList($auction,'weighting',array('No'=>'No','Yes'=>'Yes'),array('onchange'=>'auctionWeighting(this.value);','class'=>'form-control')); ?>
                   <?php echo $form->error($auction,'weighting'); ?>
            </div>
            <div class="nodisplay auction-weighting">
                <div class="form-group">
                        <?php echo $form->labelEx($auction,'price_weight'); ?>
                        <?php $range = range(0, 100);
                        echo $form->dropDownList($auction,'price_weight',$range,array('prompt'=>'%','class'=>'form-control')); ?>
                        <?php echo $form->error($auction,'price_weight'); ?>
                </div>
                <div class="form-group">
                        <?php echo $form->labelEx($auction,'q_id'); ?>
                        <?php
                        $qid_range[0] = 'Slect Questionair';
                        $selectedqid = 0;
                        if(!empty($questionaire)){
                            $selectedqid  = $auction->q_id;
                            foreach ($questionaire as $qeustion){
                                 
                                $qid_range[$qeustion['title']] = $qeustion['title'];
                                if($selectedqid  =$qeustion['ID']){
                                    $selectedqid = $qeustion['title'];
                                }
                            }
                           
//                            echo $selectedqid;exit;
                        }
                        
                        echo $form->dropDownList($auction,'q_id',$qid_range,
                                array(
                            'class'=>'form-control',
                            'options'=>array($selectedqid=>array('selected'=>true))       
                                )    ); ?>
                        <?php echo $form->error($auction,'q_id'); ?>
                </div>
                <div class="row nodisplay">
                        <?php echo $form->labelEx($auction,'q_percentage'); ?>
                        <?php echo $form->textField($auction,'q_percentage',array('class'=>'form-control')); ?>
                        <?php echo $form->error($auction,'q_percentage'); ?>
                </div>
            </div>
        </div>
        <div id="rfq" class="nodisplay">
            <h2>RFQ</h2>
        <div class="form-group">
		<?php echo $form->labelEx($rfq,'deadline'); ?>
		<?php echo $form->textField($rfq,'deadline',array('rows'=>6, 'cols'=>50,'class'=>'datetimepicker form-control')); ?>
		<?php echo $form->error($rfq,'deadline'); ?>
	</div>
        <div class="form-group">
		<?php echo $form->labelEx($rfq,'bid_direction'); ?>
		<?php echo $form->dropDownList($rfq,'bid_direction',array(1=>'Reverse',2=>'Forward'),array("class"=>'form-control')); ?>
		<?php echo $form->error($rfq,'bid_direction'); ?>
	</div>
        <div class="form-group">
		<?php echo $form->labelEx($rfq,'weighting'); ?>
		<?php echo $form->dropDownList($rfq,'weighting',array(0=>'No',1=>'Yes'),array('class'=>'form-control','onchange'=>'changerfqWeighting(this.value)')); ?>
		<?php echo $form->error($rfq,'weighting'); ?>
	</div>
        <div class="nodisplay rfq-weighting">
            <div class="form-group ">
                    <?php echo $form->labelEx($rfq,'price_weight'); ?>
                    <?php $range = range(0, 100);
                    echo $form->dropDownList($rfq,'price_weight',$range,array('prompt'=>'%','class'=>'form-control')); ?>
                    <?php echo $form->error($rfq,'price_weight'); ?>
            </div>
            <div class="form-group">
                    <?php echo $form->labelEx($rfq,'q_id'); ?>
                    <?php $range = range(0, 100);
                    echo $form->dropDownList($rfq,'q_id',array(0=>'Slect Questionair'),array('class'=>'form-control')); ?>
                    <?php echo $form->error($rfq,'q_id'); ?>
            </div>
            <div class="row nodisplay">
                    <?php echo $form->labelEx($rfq,'q_percentage'); ?>
                    <?php echo $form->textField($rfq,'q_percentage',array('class'=>'form-control')); ?>
                    <?php echo $form->error($rfq,'q_percentage'); ?>
            </div>
        </div>
        </div>
        <div class="form-group">
		<?php echo $form->labelEx($model,'tiedbid'); ?>
		<?php echo $form->dropDownList($model,'tiedbid',array(1=>'Equal',2=>'Best',3=>'Seperate'),array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'tiedbid'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

    </div>
    
</div><!-- form -->
