<?php
/* @var $this EventsController */
/* @var $model Events */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'event_title'); ?>
		<?php echo $form->textField($model,'event_title',array('size'=>60,'maxlength'=>350)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'event_description'); ?>
		<?php echo $form->textArea($model,'event_description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seal_result'); ?>
		<?php echo $form->textField($model,'seal_result'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'multi_currency'); ?>
		<?php echo $form->textField($model,'multi_currency'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'event_type'); ?>
		<?php echo $form->textField($model,'event_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'eventcode'); ?>
		<?php echo $form->textField($model,'eventcode',array('size'=>60,'maxlength'=>350)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tiedbid'); ?>
		<?php echo $form->textField($model,'tiedbid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createdat'); ?>
		<?php echo $form->textField($model,'createdat'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->