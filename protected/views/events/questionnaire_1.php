<?php
/* @var $this EventsController */
/* @var $model Events */
/* @var $form CActiveForm */
?>

<div class="form row">
    <div class='col-sm-8'>
        
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'events-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
       
	
	<?php 
//        echo '<pre>'; print_r($model);exit;
    
        if($form->errorSummary($model)){?>
<div class="alert alert-danger">
  <strong>Danger!</strong>
      <?php 
        echo $form->errorSummary($model);
        echo $form->errorSummary($auction);
        echo $form->errorSummary($rfq);
//        echo '<pre>';
//        print_r($model);
//        print_r($auction);exit;
        ?>
</div>
        <?php } ?>
        
        <div class='alert alert-danger nodisplay'><div class='validation-error'></div></div>
        
        <div class="row">
            <div class="section-container">
                
                <input type="hidden" value='<?php echo $model->weighting?>' name='Questionaire[weighting]' id='Questionaier_weighting' />
                <div class="section" data-sec='0'>
                    <h2>Section #1</h2>
                    <div class="form-group">
                        <label>Title</label>
                        <input  type='text' name='Sections[0][name]' class='form-control' />
                        
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea  type='text' name='Sections[0][description]' id='Sections_0_description' class='form-control wysiwygEditor' ></textarea>
                        
                    </div>
                    <?php 
                    if($model->weighting==1 || $model->weighting==3){
                    ?>
                    <div class="form-group">
                        <label>weighting</label>
                        <input  type='text' name='Sections[0][weighting]'  value='100' class='form-control' />
                    </div>    
                    <?php }?>
                    <div class="section-question">
                        <div class="questions" data-secquest='0'>
                            <div class="single-question">
                                <h1>Question1</h1>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class='form-control wysiwygEditor' id='Sections_0_Questions_0_description' name='Sections[0][Questions][0][descrption]'></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Scoring Criteria</label>
                                    <textarea class='form-control wysiwygEditor' id='Sections_0_Questions_0_qcriteria' name='Sections[0][Questions][0][qcriteria]'></textarea>
                                </div>
                                <div class='form-group'>
                                    <label>Scoring</label>
                                    <input type="text" class='form-control'  name='Sections[0][Questions][0][qscoring]' />
                                </div>
                                <?php 
                                    if($model->weighting==1 || $model->weighting==3){
                                    ?>
                                <div class='form-group'>
                                    <label>Weighting</label>
                                    <input type="text" value='100' class='form-control' name='Sections[0][Questions][0][weighting]' />
                                </div>
                                    <?php } ?>
                                <div class='form-group'>
                                    <label> Question Type</label>
                                    <select class='form-control' name='Sections[0][Questions][0][question_type]' onchange='addQuestionType(this);' >
                                        <option value=''>Please Select</option>
                                        <option value='yes_no'>Yes/No</option>
                                        <option value='pick_one_from_list'>Pick One From List</option>
                                        <option value='one_line_text'>One Line Text</option>
                                        <option value='paragraph_text'>Paragraph Text</option>
                                        <option value='document_upload'> Upload Document</option>
                                        <option value='date'>Date</option>
                                        <option value='table'>Table</option>
                                    </select>
                                </div>
                                <div class='form-group question-type-container'>
                                        
                                        
                                    
                                </div>
                                
                                
                                    
                              

                                </div>
                            </div>
                            <div class='btn btn-primary' onclick='addNewQuestion(this)'>add question</div>
                        </div>
                    </div>
                
                <div class="btn btn-primary" onclick="addNewSection(this);">Add Section</div>
            </div>
                
            
        </div>
                
        </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('onclick'=>'return validateSections();')); ?>
            <div class="btn btn-success" onclick="validateSections();">Submit</div>
	</div>

<?php $this->endWidget(); ?>

    </div>
    
</div><!-- form -->
