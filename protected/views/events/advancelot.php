<?php
/* @var $this EventsController */
/* @var $model Events */
/* @var $auction Auction */
/* @var $rfq Rfq */
/* @var $form CActiveForm */
?>

<div class="form row">
    <div class='col-sm-8'>
        
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'events-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	
	<?php if($form->errorSummary($lot)){?>
<div class="alert alert-danger">
  <strong>Danger!</strong> 
      <?php 
        echo $form->errorSummary($model);
        echo $form->errorSummary($auction);
        echo $form->errorSummary($rfq);
//        echo '<pre>';
//        print_r($model);
//        print_r($auction);exit;
        ?>
</div>
        <?php } ?>
	
       
        <div class="form-group">
		<?php echo $form->labelEx($lot,'lotname'); ?>
		<?php echo $form->textField($lot,'lotname',array('size'=>60,'maxlength'=>350 ,'class'=>'form-control')); ?>
		<?php echo $form->error($lot,'lotname'); ?>
	</div>


        
	<div class="row buttons">
		<?php echo CHtml::submitButton($lot->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
        
        <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'events-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	
        <div class='row'>
            <div class='col-sm-12'>
                <?php 
               if(isset($exlots)){
                ?>
                <h1><?php echo $exlots->lotname?></h1>
                <?php 
                } 
               ?>
                <div class='form-group'>
                    <label>Name</label>
                    <input class='form-control' name='LotsComponent[name]'type="text" />
                </div>
                <div class='form-group'>
                    <label>Description</label>
                    <input class='form-control' name='LotsComponent[description]' type="text" />
                </div>
                <div class='form-group  '>
                    <label class='radio-inline' > <input class='form-control' name='LotsComponent[entered_by]' value='0' id='LotsComponent_entered_by_0' type='radio' /> Host</label>
                    
                    <label class='radio-inline'><input class='form-control' name='LotsComponent[entered_by]' value='1' id='LotsComponent_entered_by_1' type='radio' /> Participant</label>
                    
                </div>
                <div class="form-group ">
                    <h2>component type</h2>
                    <label class='radio-inline'> <input  class='form-control' name='LotsComponent[field_type]' value='0' id='LotsComponent_field_type_0' type='radio' /> Price</label>
                    
                    <label class='radio-inline'> <input class='form-control' name='LotsComponent[field_type]' value='1' id='LotsComponent_field_type_1' type='radio' />Text</label>
                    
                    <label class='radio-inline'> <input class='form-control' name='LotsComponent[field_type]' value='2' id='LotsComponent_field_type_2' type='radio' />Number</label>
                    
                    <label class='radio-inline'> <input class='form-control' name='LotsComponent[field_type]' value='3' id='LotsComponent_field_type_3' type='radio' />Date</label>
                    
                    <label class='radio-inline'> <input class='form-control' name='LotsComponent[field_type]' value='4' id='LotsComponent_field_type_4' type='radio' />Pick List</label>
                    
                </div>
                <div class='pick-list nodisplay'>
                    
                </div>
                <div class='price-sec nodisplay'>
                    <div class='form-group'>
                        <label>Decimal places</label>
                        <select class='form-control' name='LotsComponent[decimal_places]'  >     
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class='form-group '>    
                        <label  class='radio-inline'>Free text <input class='form-control' name='LotsComponent[uom]' value='0'  type='radio' /></label>
                    
                        <label class='radio-inline'>Enter default <input class='form-control' name='LotsComponent[uom]' value='1'  type='radio' /></label>
                    
                    <label class='radio-inline'>Use UoM Set <input class='form-control' name='LotsComponent[uom]' value='2'  type='radio' /></label>
                    <div class='default-mou nodisplay'>
                        <label>Enter Default Uom</label>
                        <input type='text' name='LotsComponent[default_uom]' class='form-control' />
                    </div>
                    <div class='set-mou nodisplay'>
                        <div class='form-group'>
                            <label> Select Uom</label>
                            <select name='LotsComponent[uom_set]' class='form-control' >
                                <option>electricity</option>
                                <option>length</option>
                                <option>each</option>
                            </select>   
                        </div>
                        <div class='form-group'>
                            <label> Default Uom</label>
                            <select name='LotsComponent[uom_measure]' class='form-control' >
                                <option>electricity</option>
                                <option>length</option>
                                <option>each</option>
                            </select>   
                        </div>
                        
                    </div>
                    </div>
                </div>
                <div class='pick-list nodisplay'>
                        <table class='table'>
                            <thead>
                                <tr>
                                    <td>Option</td>
                                    <td>Value</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr data-picklist='0'>
                                    <td>
                                        <input type='text' class='form-control' placeholder='Option' name='LotsComponent[pick_list][0][option]' />
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' placeholder='Value' name='LotsComponent[pick_list][0][value]' />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class='btn btn-default' onclick='addPickListField(this);'>Add Field</div> 
                   
                </div>
                
            </div>
        </div>
        	<div class="row buttons">
		<?php echo CHtml::submitButton($lot->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
	</div>
        <?php $this->endWidget(); ?>
    </div>
    
</div><!-- form -->
