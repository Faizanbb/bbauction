<?php
/* @var $this EventsController */
/* @var $data Events */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event_title')); ?>:</b>
	<?php echo CHtml::encode($data->event_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event_description')); ?>:</b>
	<?php echo CHtml::encode($data->event_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seal_result')); ?>:</b>
	<?php echo CHtml::encode($data->seal_result); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('multi_currency')); ?>:</b>
	<?php echo CHtml::encode($data->multi_currency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event_type')); ?>:</b>
	<?php echo CHtml::encode($data->event_type); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('eventcode')); ?>:</b>
	<?php echo CHtml::encode($data->eventcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tiedbid')); ?>:</b>
	<?php echo CHtml::encode($data->tiedbid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdat')); ?>:</b>
	<?php echo CHtml::encode($data->createdat); ?>
	<br />

	*/ ?>

</div>