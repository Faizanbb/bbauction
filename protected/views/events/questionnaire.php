<?php
/* @var $this EventsController */
/* @var $model Events */
/* @var $form CActiveForm */
?>

<div class="form row">
    <div class='col-sm-8'>
        
<?php 
//echo '<pre>';
//print_r($sectionsQuestion);exit;
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'events-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
       
	
	<?php 
//        echo '<pre>'; print_r($model);exit;
    
        if($form->errorSummary($model)){?>
<div class="alert alert-danger">
  <strong>Danger!</strong>
      <?php 
        echo $form->errorSummary($model);
        echo $form->errorSummary($auction);
        echo $form->errorSummary($rfq);
//        echo '<pre>';
//        print_r($model);
//        print_r($auction);exit;
        ?>
</div>
        <?php } ?>
        
        <div class='alert alert-danger nodisplay'><div class='validation-error'></div></div>
        
        <div class="row">
            <div class="section-container">
                
                <input type="hidden" value='<?php echo $model->weighting?>' name='Questionaire[weighting]' id='Questionaier_weighting' />
               
                <?php 
                if(isset($sectionsQuestion)&& !empty($sectionsQuestion)){
                $count = 0;
                foreach($sectionsQuestion as $sq){?>
                <div class="section" data-sec='<?php echo $count;?>'>
                    <h2>Section #<?php echo $count+1;?></h2>
                    <div><button class='btn btn-danger' onclick='deleteSection(this)'>Delete</button></div>
                    <div class="form-group">
                        <label>Title</label>
                        <input  type='text' name='Sections[<?php echo $count; ?>][name]' value='<?php echo $sq->name?>' class='form-control' />
                        
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea  type='text' name='Sections[<?php echo $count; ?>][description]' id='Sections_<?php echo $count;?>_description' class='form-control wysiwygEditor' ><?php echo $sq->description?></textarea>
                        
                    </div>
                    <?php 
                    if($model->weighting==1 || $model->weighting==3){
                    ?>
                    <div class="form-group">
                        <label>weighting</label>
                        <input  type='text' name='Sections[<?php echo $count; ?>][weighting]'  value='<?php echo $sq->weighting?>' class='form-control' />
                    </div>    
                    <?php }?>
                    <div class="section-question">
                        <?php $qcount = 0; foreach($sq->questions as $sqq){ ?>
                        <div class="questions" data-secquest='<?php echo $qcount;?>'>
                            <div class="single-question">
                                <h1> Question # <?php echo $qcount+1;?></h1>
                                <div><button class="btn btn-danger" onclick="deleteQuestion(this)">Delete</button></div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class='form-control wysiwygEditor' id='Sections_<?php echo $count;?>_Questions_<?php echo $qcount;?>_description' name='Sections[<?php echo $count; ?>][Questions][<?php echo $qcount;?>][descrption]'><?php echo $sqq->description;?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Scoring Criteria</label>
                                    <textarea class='form-control wysiwygEditor' id='Sections_<?php echo $count;?>_Questions_<?php echo $qcount;?>_qcriteria' name='Sections[<?php echo $count; ?>][Questions][<?php echo $qcount; ?>][qcriteria]'><?php echo $sqq->qcriteria;?></textarea>
                                </div>
                                <div class='form-group'>
                                    <label>Scoring</label>
                                    <input type="text" class='form-control' value='<?php echo $sqq->qscoring?>' name='Sections[<?php echo $count; ?>][Questions][<?php echo $qcount; ?>][qscoring]' />
                                </div>
                                <?php 
                                    if($model->weighting==1 || $model->weighting==3){
                                    ?>
                                <div class='form-group'>
                                    <label>Weighting</label>
                                    <input type="text" value='<?php echo $sqq->weighting; ?>' class='form-control' name='Sections[<?php echo $count; ?>][Questions][<?php echo $qcount; ?>][weighting]' />
                                </div>
                                    <?php } ?>
                                <div class='form-group'>
                                    <label> Question Type</label>
                                    <select class='form-control' value='<?php echo $sqq->question_type; ?>' name='Sections[<?php echo $count; ?>][Questions][<?php echo $qcount; ?>][question_type]' onchange='addQuestionType(this);' >
                                        <option value=''>Please Select</option>
                                        <option value='yes_no'>Yes/No</option>
                                        <option value='pick_one_from_list'>Pick One From List</option>
                                        <option value='one_line_text'>One Line Text</option>
                                        <option value='paragraph_text'>Paragraph Text</option>
                                        <option value='document_upload'> Upload Document</option>
                                        <option value='date'>Date</option>
                                        <option value='table'>Table</option>
                                    </select>
                                </div>
                                <div class='form-group question-type-container'>
                                        
                                         <?php
                                         if($sqq->question_type=='yes_no' ||$sqq->question_type=='pick_one_from_list'){
                                             ?>
                                         <div class='yes-no-question'>
                                            <table class='table'>
                                               <thead>
                                                  <tr>
                                                     <td></td>
                                                     <td>Scoring</td>
                                                     <td>Fail</td>
                                                  </tr>
                                               </thead>
                                               <tbody>
                                                    <?php 
                                                    $qaqcount = 0;
                                              foreach($sqq->questionsAnswers as $sqqa){ ?>
                                    
                                                  <tr>
                                                      <td><input type='text'  class='form-control'  value='<?php echo $sqqa->q_ans;?>' name='Sections[<?php echo $count?>][Questions][<?php echo $qcount?>][QuestionAnswers][<?php echo $qaqcount;?>][option]'  /></td>
                                                     <td><input type='text' value='<?php echo $sqqa->score;?>' class='form-control' name='Sections[<?php echo $count?>][Questions][<?php echo $qcount?>][QuestionAnswers][<?php echo $qaqcount;?>][scoring]' /></td>
                                                     <td>
                                                        <select value='<?php echo $sqqa->fail_on;?>'class='form-control' name='Sections[<?php echo $count?>][Questions][<?php echo $qcount?>][QuestionAnswers][<?php echo $qaqcount;?>][fail]' >
                                                           <option>No</option>
                                                           <option>Yes</option>
                                                        </select>
                                                     </td>
                                                  </tr>
                                                   <?php $qaqcount++;} 
                                                 
                                                    ?>  
                                                </tbody>
                                            </table>
                                         </div>
                                             <?php }
                                                    ?>
                                               
                                </div>
                                
                            </div>
                        </div>
                        <?php $qcount++;} ?>
                        <div class='btn btn-primary' onclick='addNewQuestion(this)'>add question</div>
                    </div>
                
                </div>
                <?php $count++; }
                }else{
                ?>
                <div class="section" data-sec='0'>
                    <h2>Section #1</h2>
                    <div class="form-group">
                        <label>Title</label>
                        <input  type='text' name='Sections[0][name]' class='form-control' />
                        
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea  type='text' name='Sections[0][description]' id='Sections_0_description' class='form-control wysiwygEditor' ></textarea>
                        
                    </div>
                    <?php 
                    if($model->weighting==1 || $model->weighting==3){
                    ?>
                    <div class="form-group">
                        <label>weighting</label>
                        <input  type='text' name='Sections[0][weighting]'  value='100' class='form-control' />
                    </div>    
                    <?php }?>
                    <div class="section-question">
                        <div class="questions" data-secquest='0'>
                            <div class="single-question">
                                <h1>Question1</h1>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class='form-control wysiwygEditor' id='Sections_0_Questions_0_description' name='Sections[0][Questions][0][descrption]'></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Scoring Criteria</label>
                                    <textarea class='form-control wysiwygEditor' id='Sections_0_Questions_0_qcriteria' name='Sections[0][Questions][0][qcriteria]'></textarea>
                                </div>
                                <div class='form-group'>
                                    <label>Scoring</label>
                                    <input type="text" class='form-control'  name='Sections[0][Questions][0][qscoring]' />
                                </div>
                                <?php 
                                    if($model->weighting==1 || $model->weighting==3){
                                    ?>
                                <div class='form-group'>
                                    <label>Weighting</label>
                                    <input type="text" value='100' class='form-control' name='Sections[0][Questions][0][weighting]' />
                                </div>
                                    <?php } ?>
                                <div class='form-group'>
                                    <label> Question Type</label>
                                    <select class='form-control' name='Sections[0][Questions][0][question_type]' onchange='addQuestionType(this);' >
                                        <option value=''>Please Select</option>
                                        <option value='yes_no'>Yes/No</option>
                                        <option value='pick_one_from_list'>Pick One From List</option>
                                        <option value='one_line_text'>One Line Text</option>
                                        <option value='paragraph_text'>Paragraph Text</option>
                                        <option value='document_upload'> Upload Document</option>
                                        <option value='date'>Date</option>
                                        <option value='table'>Table</option>
                                    </select>
                                </div>
                                <div class='form-group question-type-container'>
                                        
                                        
                                    
                                </div>
                                
                                
                                    
                              

                            </div>
                        </div>
                        <div class='btn btn-primary' onclick='addNewQuestion(this)'>add question</div>
                    </div>
                </div>
                <?php } ?>
                <div class="btn btn-primary" onclick="addNewSection(this);">Add Section</div>
            </div>
                
            
        </div>
                
        </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('onclick'=>'return validateSections();')); ?>
            <div class="btn btn-success" onclick="validateSections();">Submit</div>
	</div>

<?php $this->endWidget(); ?>

    </div>
    
</div><!-- form -->
