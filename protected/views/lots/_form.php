<?php
/* @var $this LotsController */
/* @var $model Lots */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lots-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>
	
	<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type}' style='color:red;'>{$flash}</div>";
		}
	?>
	<br><br>
	<!-- <p class="note">Fields with <span class="required">*</span> are required.</p> -->

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user id'); ?>
		<?php echo $form->textField($model,'userid'); ?>
		<?php echo $form->error($model,'user id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'event id'); ?>
		<?php echo $form->textField($model,'event_id'); ?>
		<?php echo $form->error($model,'event id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lot name'); ?>
		<?php echo $form->textField($model,'lotname',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'lot name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity '); ?>
		<?php echo $form->textField($model,'quantity', array('class'=>'toCalculate')); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unit of measure'); ?>
		<?php echo $form->textField($model,'unitofmeasure',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'unit of measure'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'current price'); ?>
		<?php echo $form->textField($model,'currentprice', array('class'=>'toCalculate')); ?>
		<?php echo $form->error($model,'current price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qualification price'); ?>
		<?php echo $form->textField($model,'qualificationprice', array('class'=>'toCalculate')); ?>
		<?php echo $form->error($model,'qualification price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'current value'); ?>
		<?php echo $form->textField($model,'currentvalue', array('class'=>'total1', 'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'current value'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qualification value'); ?>
		<?php echo $form->textField($model,'qualificationvalue',array('class'=>'total2', 'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'qualification value'); ?>
	</div>

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'createdat'); ?>
		<?php //echo $form->textField($model,'createdat'); ?>
		<?php //echo $form->error($model,'createdat'); ?>
	</div> -->

	<div class="row buttons">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Cancel' : 'Cancel'); ?>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    $(document).ready(function() {
        $(".toCalculate").keyup(function() {
            var v1 = document.getElementById('Lots_quantity').value;
            var v2 = document.getElementById('Lots_currentprice').value;
            var v3 = document.getElementById('Lots_qualificationprice').value;
            
                // console.log(v1*v2);
            $(".total1").val(v1*v2);
            $(".total2").val(v1*v3);
        });
    });

</script>
<script type="text/javascript">
	
</script>