<?php
/* @var $this LotsController */
/* @var $model Lots */

$this->breadcrumbs=array(
	'Lots'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Lots', 'url'=>array('index')),
	array('label'=>'Create Lots', 'url'=>array('create')),
	array('label'=>'View Lots', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Lots', 'url'=>array('admin')),
);
?>

<h1>Update Lot #<?php echo $model->ID; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>