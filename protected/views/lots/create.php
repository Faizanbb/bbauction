<?php
/* @var $this LotsController */
/* @var $model Lots */

$this->breadcrumbs=array(
	'Lots'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Lots', 'url'=>array('index')),
	array('label'=>'Manage Lots', 'url'=>array('admin')),
);

?>

<h1>Add New Lot</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>