<?php
/* @var $this LotsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Lots',
);

$this->menu=array(
	array('label'=>'Create Lots', 'url'=>array('create')),
	array('label'=>'Manage Lots', 'url'=>array('admin')),
);
?>

<h1>Lots</h1>

	<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type}' style='color:red;'>{$flash}</div>";
		}
	?>
	<br><br>
	
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
