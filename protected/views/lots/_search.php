<?php
/* @var $this LotsController */
/* @var $model Lots */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'event_id'); ?>
		<?php echo $form->textField($model,'event_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lotname'); ?>
		<?php echo $form->textField($model,'lotname',array('size'=>60,'maxlength'=>300)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unitofmeasure'); ?>
		<?php echo $form->textField($model,'unitofmeasure',array('size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'currentprice'); ?>
		<?php echo $form->textField($model,'currentprice'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qualificationprice'); ?>
		<?php echo $form->textField($model,'qualificationprice'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'currentvalue'); ?>
		<?php echo $form->textField($model,'currentvalue'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qualificationvalue'); ?>
		<?php echo $form->textField($model,'qualificationvalue'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createdat'); ?>
		<?php echo $form->textField($model,'createdat'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->