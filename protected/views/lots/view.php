<?php
/* @var $this LotsController */
/* @var $model Lots */

$this->breadcrumbs=array(
	'Lots'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Lots', 'url'=>array('index')),
	array('label'=>'Create Lots', 'url'=>array('create')),
	array('label'=>'Update Lots', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Lots', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>('Are you sure you want to delete this lot?'))),
	array('label'=>'Manage Lots', 'url'=>array('admin')),
);
?>

<h1>View Lot #<?php echo $model->ID; ?></h1>

<?php 
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'userid',
		'event_id',
		'lotname',
		'quantity',
		'unitofmeasure',
		'currentprice',
		'qualificationprice',
		'currentvalue',
		'qualificationvalue',
		'createdat',
	),
)); 

?>
