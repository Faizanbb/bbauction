<?php
/* @var $this LotsController */
/* @var $data Lots */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event_id')); ?>:</b>
	<?php echo CHtml::encode($data->event_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lotname')); ?>:</b>
	<?php echo CHtml::encode($data->lotname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unitofmeasure')); ?>:</b>
	<?php echo CHtml::encode($data->unitofmeasure); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currentprice')); ?>:</b>
	<?php echo CHtml::encode($data->currentprice); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('qualificationprice')); ?>:</b>
	<?php echo CHtml::encode($data->qualificationprice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currentvalue')); ?>:</b>
	<?php echo CHtml::encode($data->currentvalue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qualificationvalue')); ?>:</b>
	<?php echo CHtml::encode($data->qualificationvalue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdat')); ?>:</b>
	<?php echo CHtml::encode($data->createdat); ?>
	<br />

	*/ ?>

</div>