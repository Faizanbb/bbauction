<?php
/* @var $this MessagesController */
/* @var $model Messages */

$this->breadcrumbs=array(
	'Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Messages', 'url'=>array('index')),
	array('label'=>'Manage Messages', 'url'=>array('admin')),
	// custom links
	array('label'=>'All Messages', 'url'=>array('all')),
	array('label'=>'Unread Messages', 'url'=>array('unread')),
	array('label'=>'Outbox Messages', 'url'=>array('outbox')),
);
?>

<h1>Create Messages</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>