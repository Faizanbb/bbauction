<?php
	/* @var $this MessagesController */
	/* @var $model Messages */

	$this->breadcrumbs=array(
		'Messages'=>array('index'),
		'Manage',
	);

	$this->menu=array(
		array('label'=>'List Messages', 'url'=>array('index')),
		array('label'=>'Create Messages', 'url'=>array('create')),
		array('label'=>'All Messages', 'url'=>array('all')),
		array('label'=>'Unread Messages', 'url'=>array('unread')),
		array('label'=>'Outbox Messages', 'url'=>array('outbox')),
	);

	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			$('#messages-grid').yiiGridView('update', {
				data: $(this).serialize()
			});
			return false;
		});
	");
?>

<h1>Unread Messages</h1>


<?php if(!empty($model)){?>

<div class="search-form" style="display:none">
	<?php 
		// $this->renderPartial('_search',array(
		// 	'model'=>$model,
		// )); 
	?>
</div><!-- search-form -->

<ul class="list-group">
	<?php foreach ($model as $value) {?>
		<li class="list-group-item">
			<?php echo $value['mtitle']; ?>
			&nbsp;&nbsp;&nbsp;
			<span class="pull-right">
				<span>
					<?php echo $value['createdat']; ?>
				</span>
				<?php echo CHtml::link('View', array('messages/view', 'id'=>$value->ID), array('class'=>'btn btn-warning btn-xs')); ?>
			</span>
		</li>
	<?php } ?>
</ul>

<?php 
	// $this->widget('zii.widgets.grid.CGridView', array(
	// 	'id'=>'messages-grid',
	// 	'dataProvider'=>$model->search(),
	// 	'filter'=>$model,
	// 	'columns'=>array(
	// 		'ID',
	// 		'userid',
	// 		'event_id',
	// 		'mtitle',
	// 		'mdescription',
	// 		'mread',
	// 		/*
	// 		'mtype',
	// 		'mto',
	// 		'mfrom',
	// 		'createdat',
	// 		*/
	// 		array(
	// 			'class'=>'CButtonColumn',
	// 		),
	// 	),
	// )); 
}else{
	echo '<div class="well well-small">No Unread Messages.</div>';
}
?>

