<?php
/* @var $this MessagesController */
/* @var $model Messages */
// exit('okkkk');
	$this->breadcrumbs=array(
		'Messages'=>array('index'),
		'Manage',
	);

	$this->menu=array(
		array('label'=>'List Messages', 'url'=>array('index')),
		array('label'=>'Create Messages', 'url'=>array('create')),
		// custom links
		array('label'=>'All Messages', 'url'=>array('all')),
		array('label'=>'Unread Messages', 'url'=>array('unread')),
		array('label'=>'Outbox Messages', 'url'=>array('outbox')),
	);

	Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
			$('.search-form').toggle();
			return false;
		});
		$('.search-form form').submit(function(){
			$('#messages-grid').yiiGridView('update', {
				data: $(this).serialize()
			});
			return false;
		});
	");

?>

<h1>Inbox Messages</h1>

<!-- Flash messages -->
<?php
	foreach (Yii::app()->user->getFlashes() as $type => $flash) {
		echo "<div class='{$type}' style='color:green;'>{$flash}</div>";
	}
?>
<br><br>

<?php if(!empty($model)){?>

<div class="search-form" style="display:none">
	<?php 
		// $this->renderPartial('_search',array(
		// 	'model'=>$model,
		// )); 
	?>
</div><!-- search-form -->

<ul class="list-group">
	<?php foreach ($model as $value) {?>
		<?php if ($value->mread == 1) { ?>
			<li class="list-group-item list-group-item-success" style="background: #f5f5f5;">
				<?php echo $value['mtitle']; ?>
				&nbsp;&nbsp;&nbsp;
				<span class="pull-right">
					<span>
						<?php echo $value['createdat']; ?>
					</span>
					<?php echo CHtml::link('View', array('messages/view', 'id'=>$value->ID), array('class'=>'btn btn-warning btn-xs')); ?>
				</span>
			</li>
		<?php } else{ ?>
			<li class="list-group-item">
				<?php echo $value['mtitle']; ?>
				&nbsp;&nbsp;&nbsp;
				<span class="pull-right">
					<span>
						<?php echo $value['createdat']; ?>
					</span>
					<?php echo CHtml::link('View', array('messages/view', 'id'=>$value->ID), array('class'=>'btn btn-warning btn-xs')); ?>
				</span>
			</li>
		<?php } ?>
	<?php } ?>
</ul>

<?php 
	// $this->widget('zii.widgets.grid.CGridView', array(
	// 	'id'=>'messages-grid',
	// 	'dataProvider'=>$model->search2(),
	// 	//'filter'=>$model,
	// 	'columns'=>array(
	// 		'ID',
	// 		'userid',
	// 		'event_id',
	// 		'mtitle',
	// 		'mdescription',
	// 		'mread',
	// 		/*
	// 		'mtype',
	// 		'mto',
	// 		'mfrom',
	// 		'createdat',
	// 		*/
	// 		array(
	// 			'class'=>'CButtonColumn',
	// 		),
	// 	),
	// ));

}else{
	echo '<div class="well well-small">No Inbox Messages.</div>';
}
?>
