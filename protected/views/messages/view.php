<?php
/* @var $this MessagesController */
/* @var $model Messages */

$this->breadcrumbs=array(
	'Messages'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List Messages', 'url'=>array('index')),
	array('label'=>'Create Messages', 'url'=>array('create')),
	array('label'=>'Update Messages', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Messages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Messages', 'url'=>array('admin')),
	// custom links
	array('label'=>'All Messages', 'url'=>array('all')),
	array('label'=>'Unread Messages', 'url'=>array('unread')),
	array('label'=>'Outbox Messages', 'url'=>array('outbox')),
);
?>

<h1>View Messages #<?php echo $model->ID; ?></h1>

<div class="col-md-12">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4><?php echo $model->mtitle; ?></h4>
		</div>
		<div class="panel-body">
			<p><?php echo $model->mdescription; ?></p>
		</div>
		<div class="panel-footer">
			<span><?php echo $model->createdat; ?></span>
		</div>
	</div>
</div>


<?php 
// $this->widget('zii.widgets.CDetailView', array(
// 	'data'=>$model,
// 	'attributes'=>array(
// 		'ID',
// 		'userid',
// 		'event_id',
// 		'mtitle',
// 		'mdescription',
// 		'mread',
// 		'mtype',
// 		'mto',
// 		'mfrom',
// 		'createdat',
// 	),
// )); 
?>
