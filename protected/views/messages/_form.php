<?php
/* @var $this MessagesController */
/* @var $model Messages */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'messages-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'user id'); ?>
		<?php //echo $form->textField($model,'userid'); ?>
		<?php //echo $form->error($model,'userid'); ?>
	</div> -->

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'event id'); ?>
		<?php //echo $form->textField($model,'event_id'); ?>
		<?php //echo $form->error($model,'event_id'); ?>
	</div> -->
	
	<input type="hidden" name="userid" value="1">
	<input type="hidden" name="event_id" value="7">
	<input type="hidden" name="mto" value="2">

	<div class="row">
		<?php echo $form->labelEx($model,'m title'); ?>
		<?php echo $form->textField($model,'mtitle',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'mtitle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'m description'); ?>
		<?php echo $form->textArea($model,'mdescription',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'mdescription'); ?>
	</div>

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'m read'); ?>
		<?php //echo $form->textField($model,'mread'); ?>
		<?php //echo $form->error($model,'mread'); ?>
	</div> -->

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'m type'); ?>
		<?php //echo $form->textField($model,'mtype',array('size'=>60,'maxlength'=>150)); ?>
		<?php //echo $form->error($model,'mtype'); ?>
	</div> -->
	
	<!-- <div class="row">
		<?php // echo $form->labelEx($model,'m to'); ?>
		<?php // echo $form->textField($model,'mto'); ?>
		<?php // echo $form->error($model,'mto'); ?>
	</div> -->

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'m from'); ?>
		<?php //echo $form->textField($model,'mfrom'); ?>
		<?php //echo $form->error($model,'mfrom'); ?>
	</div> -->

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'createdat'); ?>
		<?php //echo $form->textField($model,'createdat'); ?>
		<?php //echo $form->error($model,'createdat'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->