<?php
/* @var $this MessagesController */
/* @var $data Messages */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::encode($data->userid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event_id')); ?>:</b>
	<?php echo CHtml::encode($data->event_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mtitle')); ?>:</b>
	<?php echo CHtml::encode($data->mtitle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mdescription')); ?>:</b>
	<?php echo CHtml::encode($data->mdescription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mread')); ?>:</b>
	<?php echo CHtml::encode($data->mread); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mtype')); ?>:</b>
	<?php echo CHtml::encode($data->mtype); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mto')); ?>:</b>
	<?php echo CHtml::encode($data->mto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mfrom')); ?>:</b>
	<?php echo CHtml::encode($data->mfrom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdat')); ?>:</b>
	<?php echo CHtml::encode($data->createdat); ?>
	<br />

	*/ ?>

</div>