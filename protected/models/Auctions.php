<?php

/**
 * This is the model class for table "auctions".
 *
 * The followings are the available columns in table 'auctions':
 * @property integer $ID
 * @property integer $userid
 * @property integer $event_id
 * @property string $direction
 * @property string $auction
 * @property integer $minduration
 * @property integer $dynamic_close_period
 * @property integer $applies_to
 * @property integer $min_bid_changes
 * @property integer $max_bid_change
 * @property string $weighting
 * @property integer $price_weight
 * @property integer $q_id
 * @property integer $q_percentage
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Events $event
 */
class Auctions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $auctionstarttime;
	public function tableName()
	{
		return 'auctions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, event_id, starttime,direction, auction, minduration, dynamic_close_period, min_bid_changes, max_bid_change', 'required'),
			array('userid, event_id, minduration, dynamic_close_period, applies_to, min_bid_changes, max_bid_change, price_weight, q_id, q_percentage', 'numerical', 'integerOnly'=>true),
			array('direction', 'length', 'max'=>7),
			array('auction', 'length', 'max'=>8),
			array('weighting', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, userid, event_id, direction, auction, minduration, dynamic_close_period, applies_to, min_bid_changes, max_bid_change, weighting, price_weight, q_id, q_percentage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'event' => array(self::BELONGS_TO, 'Events', 'event_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'userid' => 'Userid',
			'event_id' => 'Event',
			'starttime' => 'Start Time',
			'direction' => 'Direction',
			'auction' => 'Auction',
			'minduration' => 'Minduration',
			'dynamic_close_period' => 'Dynamic Close Period',
			'applies_to' => 'Applies To',
			'min_bid_changes' => 'Min Bid Changes',
			'max_bid_change' => 'Max Bid Change',
			'weighting' => 'Weighting',
			'price_weight' => 'Price Weight',
			'q_id' => 'Which Questionaire',
			'q_percentage' => 'Q Percentage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('direction',$this->direction,true);
		$criteria->compare('auction',$this->auction,true);
		$criteria->compare('minduration',$this->minduration);
		$criteria->compare('dynamic_close_period',$this->dynamic_close_period);
		$criteria->compare('applies_to',$this->applies_to);
		$criteria->compare('min_bid_changes',$this->min_bid_changes);
		$criteria->compare('max_bid_change',$this->max_bid_change);
		$criteria->compare('weighting',$this->weighting,true);
		$criteria->compare('price_weight',$this->price_weight);
		$criteria->compare('q_id',$this->q_id);
		$criteria->compare('q_percentage',$this->q_percentage);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Auctions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
