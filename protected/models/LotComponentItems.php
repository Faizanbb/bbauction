<?php

/**
 * This is the model class for table "lot_component_items".
 *
 * The followings are the available columns in table 'lot_component_items':
 * @property integer $ID
 * @property integer $userid
 * @property integer $event_id
 * @property integer $lot_id
 * @property integer $lot_component_id
 * @property integer $row_id
 * @property string $title
 * @property string $description
 * @property integer $currency
 * @property integer $uom
 * @property integer $quom
 * @property integer $cpp_uom
 * @property integer $ypp_uom
 * @property integer $qualification_price
 * @property integer $total_current_value
 * @property integer $your_total_value
 * @property integer $total_qualification_value
 * @property string $answer
 * @property integer $isranked
 * @property integer $islottotal
 * @property integer $isvisibile
 */
class LotComponentItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lot_component_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' userid, event_id, lot_id, lot_component_id, row_id, title, description, currency, uom, quom, cpp_uom, ypp_uom, qualification_price, total_current_value, your_total_value, total_qualification_value, answer, isranked, islottotal, isvisibile', 'required'),
			array(' userid, event_id, lot_id, lot_component_id, row_id, currency, uom, quom, cpp_uom, ypp_uom, qualification_price, total_current_value, your_total_value, total_qualification_value, isranked, islottotal, isvisibile', 'numerical', 'integerOnly'=>true),
			array('title, answer', 'length', 'max'=>100),
			array('description', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, userid, event_id, lot_id, lot_component_id, row_id, title, description, currency, uom, quom, cpp_uom, ypp_uom, qualification_price, total_current_value, your_total_value, total_qualification_value, answer, isranked, islottotal, isvisibile', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'userid' => 'Userid',
			'event_id' => 'Event',
			'lot_id' => 'Lot',
			'lot_component_id' => 'Lot Component',
			'row_id' => 'Row',
			'title' => 'Title',
			'description' => 'Description',
			'currency' => 'Currency',
			'uom' => 'Uom',
			'quom' => 'Quom',
			'cpp_uom' => 'Cpp Uom',
			'ypp_uom' => 'Ypp Uom',
			'qualification_price' => 'Qualification Price',
			'total_current_value' => 'Total Current Value',
			'your_total_value' => 'Your Total Value',
			'total_qualification_value' => 'Total Qualification Value',
			'answer' => 'Answer',
			'isranked' => 'Isranked',
			'islottotal' => 'Islottotal',
			'isvisibile' => 'Isvisibile',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('lot_id',$this->lot_id);
		$criteria->compare('lot_component_id',$this->lot_component_id);
		$criteria->compare('row_id',$this->row_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('currency',$this->currency);
		$criteria->compare('uom',$this->uom);
		$criteria->compare('quom',$this->quom);
		$criteria->compare('cpp_uom',$this->cpp_uom);
		$criteria->compare('ypp_uom',$this->ypp_uom);
		$criteria->compare('qualification_price',$this->qualification_price);
		$criteria->compare('total_current_value',$this->total_current_value);
		$criteria->compare('your_total_value',$this->your_total_value);
		$criteria->compare('total_qualification_value',$this->total_qualification_value);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('isranked',$this->isranked);
		$criteria->compare('islottotal',$this->islottotal);
		$criteria->compare('isvisibile',$this->isvisibile);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LotComponentItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
