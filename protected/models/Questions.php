<?php

/**
 * This is the model class for table "questions".
 *
 * The followings are the available columns in table 'questions':
 * @property integer $ID
 * @property integer $userid
 * @property integer $event_id
 * @property integer $sectionid
 * @property string $description
 * @property string $qcriteria
 * @property string $qscoring
 * @property integer $mandatory
 * @property integer $weighting
 * @property string $question_type
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Events $event
 * @property Sections $section
 * @property QuestionsAnswers[] $questionsAnswers
 */
class Questions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, event_id, sectionid, question_type', 'required'),
			array('userid, event_id, sectionid, mandatory, weighting', 'numerical', 'integerOnly'=>true),
			array('question_type', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, userid, event_id, sectionid, description, qcriteria, qscoring, mandatory, weighting, question_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'event' => array(self::BELONGS_TO, 'Events', 'event_id'),
			'section' => array(self::BELONGS_TO, 'Sections', 'sectionid'),
			'questionsAnswers' => array(self::HAS_MANY, 'QuestionsAnswers', 'q_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'userid' => 'Userid',
			'event_id' => 'Event',
			'sectionid' => 'Sectionid',
			'description' => 'Description',
			'qcriteria' => 'Qcriteria',
			'qscoring' => 'Qscoring',
			'mandatory' => 'mandatory',
			'weighting' => 'Weighting',
			'question_type' => 'Question Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('sectionid',$this->sectionid);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('qcriteria',$this->qcriteria,true);
		$criteria->compare('qscoring',$this->qscoring,true);
		$criteria->compare('mandatory',$this->mandatory);
		$criteria->compare('weighting',$this->weighting);
		$criteria->compare('question_type',$this->question_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Questions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
