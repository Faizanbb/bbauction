<?php

/**
 * This is the model class for table "lots_component".
 *
 * The followings are the available columns in table 'lots_component':
 * @property integer $ID
 * @property integer $userid
 * @property integer $event_id
 * @property integer $lotid
 * @property string $name
 * @property string $description
 * @property integer $entered_by
 * @property integer $field_type
 * @property integer $decimal_places
 * @property string $uom_set
 * @property string $uom_measure
 * @property string $pick_list_json
 */
class LotsComponent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lots_component';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, event_id, lotid, name, entered_by, field_type', 'required'),
			array(' userid, event_id, lotid, entered_by, field_type, decimal_places', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('description', 'length', 'max'=>200),
			array('pick_list_json', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, userid, event_id, lotid, name, description, entered_by, field_type, decimal_places, uom_set, uom_measure, pick_list_json', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'userid' => 'Userid',
			'event_id' => 'Event',
			'lotid' => 'Lotid',
			'name' => 'Name',
			'description' => 'Description',
			'entered_by' => 'Entered By',
			'field_type' => 'Field Type',
			'decimal_places' => 'Decimal Places',
			'uom_set' => 'Uom Set',
			'uom_measure' => 'Uom Measure',
			'pick_list_json' => 'Pick List Json',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('lotid',$this->lotid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('entered_by',$this->entered_by);
		$criteria->compare('field_type',$this->field_type);
		$criteria->compare('decimal_places',$this->decimal_places);
		$criteria->compare('uom_set',$this->uom_set,true);
		$criteria->compare('uom_measure',$this->uom_measure,true);
		$criteria->compare('pick_list_json',$this->pick_list_json,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LotsComponent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
