<?php

/**
 * This is the model class for table "events".
 *
 * The followings are the available columns in table 'events':
 * @property integer $ID
 * @property integer $userid
 * @property string $event_title
 * @property string $event_description
 * @property integer $seal_result
 * @property integer $multi_currency
 * @property integer $event_type
 * @property string $eventcode
 * @property integer $tiedbid
 * @property integer $questionnaire
 * @property string $createdat
 *
 * The followings are the available model relations:
 * @property Auctions[] $auctions
 * @property Currency[] $currencies
 * @property Documents[] $documents
 * @property Users $user
 * @property Lots[] $lots
 * @property Messages[] $messages
 * @property Questionnaire[] $questionnaires
 * @property Questions[] $questions
 * @property QuestionsAnswers[] $questionsAnswers
 * @property Rfq[] $rfqs
 * @property Sections[] $sections
 */
class Events extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
        public $add_questionaire;
	public function tableName()
	{
		return 'events';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, event_title, event_description, seal_result, multi_currency, event_type, eventcode, tiedbid, createdat', 'required'),
			array('userid, seal_result, multi_currency, event_type, tiedbid,questionnaire', 'numerical', 'integerOnly'=>true),
			array('event_title, eventcode', 'length', 'max'=>350),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, userid, event_title, event_description, seal_result, multi_currency, event_type, eventcode, tiedbid, createdat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'auctions' => array(self::HAS_MANY, 'Auctions', 'event_id'),
			'currencies' => array(self::HAS_MANY, 'Currency', 'event_id'),
			'documents' => array(self::HAS_MANY, 'Documents', 'event_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'lots' => array(self::HAS_MANY, 'Lots', 'event_id'),
			'messages' => array(self::HAS_MANY, 'Messages', 'event_id'),
			'questionnaires' => array(self::HAS_MANY, 'Questionnaire', 'event_id'),
			'questions' => array(self::HAS_MANY, 'Questions', 'event_id'),
			'questionsAnswers' => array(self::HAS_MANY, 'QuestionsAnswers', 'event_id'),
			'rfqs' => array(self::HAS_MANY, 'Rfq', 'event_id'),
			'sections' => array(self::HAS_MANY, 'Sections', 'event_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'userid' => 'Userid',
			'event_title' => 'Event Title',
			'event_description' => 'Event Description',
			'seal_result' => 'Seal Result',
			'multi_currency' => 'Multi Currency',
			'event_type' => 'Event Type',
			'eventcode' => 'Eventcode',
			'tiedbid' => 'Tiedbid',
			'createdat' => 'Createdat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('event_title',$this->event_title,true);
		$criteria->compare('event_description',$this->event_description,true);
		$criteria->compare('seal_result',$this->seal_result);
		$criteria->compare('multi_currency',$this->multi_currency);
		$criteria->compare('event_type',$this->event_type);
		$criteria->compare('eventcode',$this->eventcode,true);
		$criteria->compare('tiedbid',$this->tiedbid);
		$criteria->compare('createdat',$this->createdat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Events the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
