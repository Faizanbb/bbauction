<?php

/**
 * This is the model class for table "questions_answers".
 *
 * The followings are the available columns in table 'questions_answers':
 * @property integer $ID
 * @property integer $userid
 * @property integer $event_id
 * @property integer $q_id
 * @property string $q_ans
 * @property integer $score
 * @property string $fail_on
 * @property integer $num_of_allowed_txt
 * @property string $jason_table
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Events $event
 * @property Questions $q
 */
class QuestionsAnswers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'questions_answers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, event_id, q_id', 'required'),
			array('userid, event_id, q_id, score, num_of_allowed_txt', 'numerical', 'integerOnly'=>true),
			array('q_ans', 'length', 'max'=>250),
			array('fail_on', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, userid, event_id, q_id, q_ans, score, fail_on, num_of_allowed_txt, jason_table', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'event' => array(self::BELONGS_TO, 'Events', 'event_id'),
			'q' => array(self::BELONGS_TO, 'Questions', 'q_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'userid' => 'Userid',
			'event_id' => 'Event',
			'q_id' => 'Q',
			'q_ans' => 'Q Ans',
			'score' => 'Score',
			'fail_on' => 'Fail On',
			'num_of_allowed_txt' => 'Num Of Allowed Txt',
			'jason_table' => 'Jason Table',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('q_id',$this->q_id);
		$criteria->compare('q_ans',$this->q_ans,true);
		$criteria->compare('score',$this->score);
		$criteria->compare('fail_on',$this->fail_on,true);
		$criteria->compare('num_of_allowed_txt',$this->num_of_allowed_txt);
		$criteria->compare('jason_table',$this->jason_table,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuestionsAnswers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
