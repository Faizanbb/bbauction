<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $ID
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $company
 * @property string $country
 * @property string $timezone
 * @property integer $usertype
 * @property string $industry
 * @property integer $termsaccepted
 * @property string $createdat
 *
 * The followings are the available model relations:
 * @property Auctions[] $auctions
 * @property Currency[] $currencies
 * @property Documents[] $documents
 * @property Events[] $events
 * @property Lots[] $lots
 * @property Messages[] $messages
 * @property Payments[] $payments
 * @property Questionnaire[] $questionnaires
 * @property Questions[] $questions
 * @property QuestionsAnswers[] $questionsAnswers
 * @property Rfq[] $rfqs
 * @property Sections[] $sections
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, username, password, email, company, country, timezone, usertype, industry, termsaccepted, createdat', 'required'),
			array('usertype, termsaccepted', 'numerical', 'integerOnly'=>true),
			array('name, username, industry', 'length', 'max'=>250),
			array('password, email, company, country', 'length', 'max'=>350),
			array('timezone', 'length', 'max'=>150),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, name, username, password, email, company, country, timezone, usertype, industry, termsaccepted, createdat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'auctions' => array(self::HAS_MANY, 'Auctions', 'userid'),
			'currencies' => array(self::HAS_MANY, 'Currency', 'userid'),
			'documents' => array(self::HAS_MANY, 'Documents', 'userid'),
			'events' => array(self::HAS_MANY, 'Events', 'userid'),
			'lots' => array(self::HAS_MANY, 'Lots', 'userid'),
			'messages' => array(self::HAS_MANY, 'Messages', 'userid'),
			'payments' => array(self::HAS_MANY, 'Payments', 'userid'),
			'questionnaires' => array(self::HAS_MANY, 'Questionnaire', 'userid'),
			'questions' => array(self::HAS_MANY, 'Questions', 'userid'),
			'questionsAnswers' => array(self::HAS_MANY, 'QuestionsAnswers', 'userid'),
			'rfqs' => array(self::HAS_MANY, 'Rfq', 'userid'),
			'sections' => array(self::HAS_MANY, 'Sections', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'name' => 'Name',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'company' => 'Company',
			'country' => 'Country',
			'timezone' => 'Timezone',
			'usertype' => 'Usertype',
			'industry' => 'Industry',
			'termsaccepted' => 'Termsaccepted',
			'createdat' => 'Createdat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('timezone',$this->timezone,true);
		$criteria->compare('usertype',$this->usertype);
		$criteria->compare('industry',$this->industry,true);
		$criteria->compare('termsaccepted',$this->termsaccepted);
		$criteria->compare('createdat',$this->createdat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
