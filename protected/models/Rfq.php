<?php

/**
 * This is the model class for table "rfq".
 *
 * The followings are the available columns in table 'rfq':
 * @property integer $ID
 * @property integer $userid
 * @property integer $event_id
 * @property string $deadline
 * @property integer $bid_direction
 * @property integer $weighting
 * @property integer $price_weight
 * @property integer $q_id
 * @property string $createdat
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Events $event
 */
class Rfq extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rfq';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, event_id, deadline, bid_direction, weighting, createdat', 'required'),
			array('userid, event_id, bid_direction, weighting,price_weight', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, userid, event_id, deadline, bid_direction, weighting, createdat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'event' => array(self::BELONGS_TO, 'Events', 'event_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'userid' => 'Userid',
			'event_id' => 'Event',
			'deadline' => 'Deadline',
			'bid_direction' => 'Bid Direction',
			'weighting' => 'Weighting',
			'price_weight' => 'Price Weighting',
			'q_id' => 'With Questionaire',
			'q_percentage' => 'Questionaire percentage',
			'createdat' => 'Createdat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('deadline',$this->deadline,true);
		$criteria->compare('bid_direction',$this->bid_direction);
		$criteria->compare('weighting',$this->weighting);
		$criteria->compare('price_weight',$this->price_weight);
		$criteria->compare('createdat',$this->createdat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Rfq the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
