<?php

/**
 * This is the model class for table "sections".
 *
 * The followings are the available columns in table 'sections':
 * @property integer $ID
 * @property integer $userid
 * @property integer $event_id
 * @property integer $qid
 * @property string $name
 * @property string $description
 * @property integer $weighting
 *
 * The followings are the available model relations:
 * @property Questions[] $questions
 * @property Users $user
 * @property Events $event
 * @property Questionnaire $q
 */
class Sections extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sections';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, event_id, qid', 'required'),
			array('userid, event_id, qid, weighting', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>350),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, userid, event_id, qid, name, description, weighting', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'questions' => array(self::HAS_MANY, 'Questions', 'sectionid'),
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'event' => array(self::BELONGS_TO, 'Events', 'event_id'),
			'q' => array(self::BELONGS_TO, 'Questionnaire', 'qid')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'userid' => 'Userid',
			'event_id' => 'Event',
			'qid' => 'Qid',
			'name' => 'Name',
			'description' => 'Description',
			'weighting' => 'Weighting',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('event_id',$this->event_id);
		$criteria->compare('qid',$this->qid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('weighting',$this->weighting);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sections the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
