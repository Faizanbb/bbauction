<?php

class EventsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','questionnaire','advancelot'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{      
                $model=new Events;
		$auction=new Auctions;
		$rfq=new Rfq;
                 $userid = 1;
		// Uncomment the following line if AJAX validation is needed
		 $this->performAjaxValidation($model);
		 $this->performAjaxValidation($auction);
		 $this->performAjaxValidation($rfq);
//        print_r($auction);exit;
		if(isset($_POST['Events']))
		{
//                    echo '<pre>';
//                    print_r($_POST);exit;
//                     foreach($_POST['Questionaire'] as $question){
//                         echo $question['deadline'];
//                     }
//                     exit;
                   
                    if(isset($_POST['Events']['ID'])&& !empty($_POST['Events']['ID']))
                      $model=   $this->loadModel($_POST['Events']['ID']);
                    
                        $createdat = date('Y-m-d H:i:s');
                	$model->attributes=$_POST['Events'];
                        $model->userid=$userid;
                	$model->createdat = $createdat;
                        $randno = $this->generateRandomString(5);
                        $model->eventcode =  $randno.$userid;
                      
                        if($model->save()){
                           
                            $eventid = $model->ID;
                            Yii::app()->user->setState("eventID",$eventid);
                            
//                            $eventid = 4;
                            $rfqweightquestion = '';
                            $auctionweightquestion = '';
                            if($model->event_type==1){
                                $rfq->attributes=$_POST['Rfq'];
                                $rfq->userid = $userid;    
                                $rfq->event_id = $eventid;    
                                $rfq->createdat = $createdat;  
                                $rfqweightquestion = $_POST['Rfq']['q_id'];
                                $rfq->q_id = 0;
//                                echo 'price weight - '.$rfq->price_weight;exit;
                                if($rfq->save())
                                    $rfqid=  $rfq->ID;
                                
                                
                            }
                            else if($model->event_type==2){
                                $auction->attributes=$_POST['Auctions'];
                                if($auction->starttime){
                                $date = date_create_from_format('Y-m-d H:m:s', $auction->starttime);
                                $starttime = $date->getTimestamp();
                                $starttime = date("Y-m-d H:i:s", $starttime);
                                $auction->starttime = $starttime;}
                                $auction->userid = $userid;
                                $auction->event_id = $eventid;
                                $auctionweightquestion = $_POST['Auctions']['q_id'];
                                $auction->q_id = 0;
                                
//                                echo 123;exit;
                                if($auction->save()){
                                    $auctionid = $auction->ID;
                                      echo '<br> Aucion Created !! <br>';
                                }
                                    
                                    
                           
                            
                            }
                                if($_POST['Events']['questionnaire']!=0){
                                    foreach($_POST['Questionaire'] as $question){
                                        $questionaire=new Questionnaire;
                                        $questionaire->userid = $userid;
                                        $questionaire->event_id = $eventid;
                                        $questionaire->title = $question['title'];
                                        $questionaire->deadline = $question['deadline'];
                                        if(isset($question['with_scoring']) && $question['with_scoring']=='on')
                                            $questionaire->scoring = 1;
                                        else
                                            $questionaire->scoring = 0;
                                        $questionaire->weighting = $question['with_weight'];
                                        $questionaire->createdat = $createdat;
                                        if(!isset($question['with_prequalification']))
                                          $questionaire->pre_qualify = 0;
                                        else
                                          $questionaire->pre_qualify = 1;
                                            
                                        $questionaire->createdat = $createdat;
                                        
                                        if(!$questionaire->save())
                                            print_r($questionaire->getErrors());
                                        
                                        if(!empty($auctionweightquestion)&&isset($auctionid)&&!empty($auctionid)){
                                            if($question['title']==$auctionweightquestion){
                                               $auction =  $this->loadAuctionsModel($auctionid);
                                                $auction->q_id = $questionaire->ID;
                                                $auction->save();
                                            }
                                        }
                                        if(!empty($rfqweightquestion)&&isset($rfqid)&&!empty($rfqid)){
                                            if($question['title']==$rfqweightquestion){
                                                $rfq =  $this->loadRfqModel($rfqid);
                                                $rfq->q_id = $questionaire->ID;
                                                $rfq->save();
                                            }
                                        }
                                    }
                                }
                	
                            
                        }
                  echo 'event id = '.Yii::app()->user->getState("eventID");;exit;
      
		}
		$this->render('create',array(
			'model'=>$model,
			'auction'=>$auction,
			'rfq'=>$rfq,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $auction = $this->loadUpdateAuctionModel($id);
                $rfq = $this->loadUpdateRfqModel($id);
                $questionnaireModel = $this->loadQuestionaire($id);
                 $userid = 1;
                 $eventid = $id;
                 $createdat = date('Y-m-d');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Events']))
		{
			$model->attributes=$_POST['Events'];
			if($model->save()){
                            
                          if($model->event_type==1){
                                $rfq->attributes=$_POST['Rfq'];
                                $rfq->userid = $userid;    
                                $rfq->event_id = $eventid;    
                                $rfq->createdat = $createdat;  
                                $rfqweightquestion = $_POST['Rfq']['q_id'];
                                $rfq->q_id = 0;
//                                echo 'price weight - '.$rfq->price_weight;exit;
                                if($rfq->save())
                                    $rfqid=  $rfq->ID;
                                
                                
                            }
                            else if($model->event_type==2){
                                $auction->attributes=$_POST['Auctions'];
                                if($auction->starttime){
                                $date = date_create_from_format('Y-m-d H:m:s', $auction->starttime);
                                $starttime = $date->getTimestamp();
                                $starttime = date("Y-m-d H:i:s", $starttime);
                                $auction->starttime = $starttime;}
                                $auction->userid = $userid;
                                $auction->event_id = $eventid;
                                $auctionweightquestion = $_POST['Auctions']['q_id'];
                                $auction->q_id = 0;
                                
//                                echo 123;exit;
                                if($auction->save()){
                                    $auctionid = $auction->ID;
                                      echo '<br> Aucion Created !! <br>';
                                }
                                    
                                    
                           
                            
                            }
                                if($_POST['Events']['questionnaire']!=0){
                                    foreach($_POST['Questionaire'] as $question){
                                        if(isset($question['ID'])&& !empty($question['ID'])){
                                        $questionaire=$this->loadSingleQuestionnaier($question['ID']);
                                            
                                        }else
                                        $questionaire=new Questionnaire;
                                        
                                        $questionaire->userid = $userid;
                                        $questionaire->event_id = $eventid;
                                        $questionaire->title = $question['title'];
                                        $questionaire->deadline = $question['deadline'];
                                        if(isset($question['with_scoring']) && $question['with_scoring']=='on')
                                            $questionaire->scoring = 1;
                                        else
                                            $questionaire->scoring = 0;
                                        $questionaire->weighting = $question['with_weight'];
                                        $questionaire->createdat = $createdat;
                                        if(!isset($question['with_prequalification']))
                                          $questionaire->pre_qualify = 0;
                                        else
                                          $questionaire->pre_qualify = 1;
                                            
                                        $questionaire->createdat = $createdat;
                                        
                                        if(!$questionaire->save())
                                            print_r($questionaire->getErrors());
                                        
                                        if(!empty($auctionweightquestion)&&isset($auctionid)&&!empty($auctionid)){
                                            if($question['title']==$auctionweightquestion){
                                               $auction =  $this->loadAuctionsModel($auctionid);
                                                $auction->q_id = $questionaire->ID;
                                                $auction->save();
                                            }
                                        }
                                        if(!empty($rfqweightquestion)&&isset($rfqid)&&!empty($rfqid)){
                                            if($question['title']==$rfqweightquestion){
                                                $rfq =  $this->loadRfqModel($rfqid);
                                                $rfq->q_id = $questionaire->ID;
                                                $rfq->save();
                                            }
                                        }
                                    }
                                }   
                        }
                        
                        if($model->event_type==1){
                            $rfq->save();
                        }else if($model->event_type==2){
                             $auction->save();
                        }
                            
		}

		$this->render('update',array(
			'model'=>$model,
			'auction'=>$auction,
			'rfq'=>$rfq,
			'questionaire'=>$questionnaireModel,
		));
	}

//        Create Questionnaire  
        public function actionQuestionnaire($id){
            $userID = 1;
            $model = Questionnaire::model()->findByPk($id);
            $questAirID = $id;
            $eventID = $model->event_id;
            
                    
            $sectionsQuestion = $this->loadSectionQuestionData($id,$eventID);
            
            if(isset($_POST['Sections'])){
//                echo '<pre>';                print_r($_POST);exit;
                foreach($_POST['Sections'] as $section){
                    $sections = new Sections;
                    $sections->userid = $userID; 
                    $sections->event_id = $eventID; 
                    $sections->qid = $questAirID; 
                    if(isset($section['name']))
                    $sections->name = $section['name'];
                    if(isset($section['description']))
                    $sections->description = $section['description'];
                    if(isset($section['weighting']))
                    $sections->weighting = $section['weighting'];
                    if($sections->save()){
                        
                        $sectionID = $sections->ID;
                        foreach($section['Questions'] as $quest){
                            $questions = new Questions;
                            $questions->userid = $userID;
                            $questions->event_id = $eventID;
                            $questions->sectionid = $sectionID;
                            if(isset($quest['descrption']))
                            $questions->description = $quest['descrption'];
                            if(isset($quest['qcriteria']))
                            $questions->qcriteria = $quest['qcriteria'];
                            if(isset($quest['qscoring']))
                            $questions->qscoring = $quest['qscoring'];
                            if(isset($quest['mandatory']))
                            $questions->mandatory = $quest['mandatory'];
                            if(isset($quest['weighting']))
                            $questions->weighting = $quest['weighting'];
                            if(isset($quest['question_type']))
                            $questions->question_type = $quest['question_type'];
                            
                            if($questions->save()){
                                $questionID = $questions->ID;
                                if(isset($quest['QuestionAnswers'])){
                                    foreach($quest['QuestionAnswers'] as $questAnswer){
                                        $qAnswer = new QuestionsAnswers;
                                        $qAnswer->userid = $userID;
                                        $qAnswer->event_id = $eventID;
                                        $qAnswer->q_id = $questionID;
                                        if(isset($questAnswer['option']))
                                            $qAnswer->q_ans = $questAnswer['option'];
                                        if(isset($questAnswer['scoring']))
                                            $qAnswer->score = $questAnswer['scoring'];
                                        if(isset($questAnswer['fail']))
                                            $qAnswer->fail_on = $questAnswer['fail'];
                                        if(isset($questAnswer['num_of_allowed_txt']))
                                            $qAnswer->num_of_allowed_txt = $questAnswer['num_of_allowed_txt'];
                                        if($quest['question_type']=='table'){
                                            $tablejson = json_encode($quest['QuestionAnswers']['Row']);
                                            $qAnswer->jason_table = $tablejson;
                                        }
                                            

                                            $qAnswer->save(false);

                                    }
                                }
                            }
                            

                        }
                    }
                    
                }
                
                
            echo 'Done !!!';
            exit;
                
            }
            
            
            $this->render('questionnaire', array(
                'model'=>$model,
                'sectionsQuestion'=>$sectionsQuestion,
            ));
        }
        
//        Create Advanec Lot
        public function actionAdvanceLot(){
            $userId = 1;
            $eventId = 49;
            $lotId = 1;
            $lot = new Lots;
            $exsitinglot = $this->loadLots($eventId,$userId);
             if(isset($_POST['Lots'])){
                $lot->attributes=$_POST['Lots'];
                $lot->userid = $userId;
                $lot->event_id =$eventId;
                $lot->createdat = date('Y-m-d H:i:s');
                
                if($lot->save()){
                    echo $lot->ID;exit;}
                else
                echo '<pre>';print_r($lot->getErrors());exit;
             }
             if(isset($_POST['LotsComponent'])){
                echo '<pre>'; print_r($_POST);exit;
             $meta = $_POST['LotsComponent'];
                $lotc = new LotsComponent;
                $lotc->attributes=$_POST['LotsComponent'];
                $lotc->userid = $userId;
                $lotc->event_id =$eventId;
                $lotc->lotid =$lotId;
                if($meta['uom']==1)
                   $lotc->uom_measure = $meta['default_uom'] ;
                else if($meta['uom']==2){
                   $lotc->uom_set = $meta['uom_set'] ;
                   $lotc->uom_measure = $meta['uom_measure'] ;
                    
                }
                    
                if($meta['field_type']==4)
                    $lotc->pick_list_json =encode_json($meta['pick_list']);
                else
                    $lotc->pick_list_json = '';
                    
                if($lotc->save()){
                    echo $lotc->ID;exit;}
                else
                echo '<pre>';print_r($lotc->getErrors());exit;
             }
             $this->render('advancelot', array(
                'lot'=>$lot,
                'exlots'=>$exsitinglot,
            ));
        }
        
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Events');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Events('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Events']))
			$model->attributes=$_GET['Events'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Events the loaded model
	 * @throws CHttpException
	 */
        
	public function loadLots($eventId,$userid)
	{
		$model=Lots::model()->findByAttributes(array('event_id'=>$eventId,'userid'=>$userid));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadRfqModel($id)
	{
		$model=Rfq::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadUpdateRfqModel($id)
	{
		$model=Rfq::model()->findByAttributes(array('event_id'=>$id));
		if($model===null)
			$model = new Rfq;
		return $model;
	}
	public function loadQuestionaire($id)
	{
		$model=Questionnaire::model()->findAllByAttributes(array('event_id'=>$id));
//		echo '<pre>';print_r($model);exit;
                if($model===null)
			$model = new Auctions;
		return $model;
	}
	public function loadSingleQuestionnaier($id)
	{
		$model=Questionnaire::model()->findByPk($id);
//		echo '<pre>';print_r($model);exit;
                if($model===null)
			$model = new Questionnaire;
		return $model;
	}
	public function loadUpdateAuctionModel($id)
	{
		$model=Auctions::model()->findByAttributes(array('event_id'=>$id));
		if($model===null)
			$model = new Auctions;
		return $model;
	}
	public function loadAuctionsModel($id)
	{
		$model=Auctions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadModel($id)
	{
		$model=Events::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadSectionQuestionData($id,$event_id)
	{
//            echo $id;exit;
            $criteria = new CDbCriteria;
            $criteria->select = '*';
            $criteria->with = 'questions';
            $criteria->with = 'questions.questionsAnswers';
            $criteria->condition = "qid = $id";
            $model    =    Sections::model()->findAll($criteria);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Events $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='events-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
}
