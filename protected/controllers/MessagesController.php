<?php

class MessagesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			//array('allow',  // allow all users to perform 'index' and 'view' actions
				//'actions'=>array(),
				//'users'=>array('*'),
			//),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','all','unread','outbox','view'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete','admin','update','index'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	

	// All Inbox Messages
	public function actionAll()
	{
		$userID = Yii::app()->user->id;
		$model = Messages::model()->findAllByAttributes(array('mto'=>$userID));
		// echo "<pre>";
		// print_r($model);exit;
		$this->render('all',array(
			'model'=>$model,
		));
	}

	// All Unread Messages
	public function actionUnread()
	{
		$userID = Yii::app()->user->id;
		$model = Messages::model()->findAllByAttributes(array('mto'=>$userID, 'mread'=>0));

		$this->render('unread',array(
			'model'=>$model,
		));
	}

	// All Outbox Messages
	public function actionOutbox()
	{
		$userID = Yii::app()->user->id;		
		$model = Messages::model()->findAllByAttributes(array('mfrom'=>$userID));

		$this->render('outbox',array(
			'model'=>$model,
		));
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$userID=Yii::app()->user->id;
		// Get a message where MessageToID is equal to AutheticatedUserID
		$model=Messages::model()->findByAttributes(array('ID'=>$id, 'mto'=>$userID));

		if (!empty($model)) {

			$model->mread = 1;
			$type = $model->mtype;

			if (!empty($type) AND $type != 'forward,received') {
				$model->mtype = $type.',received';
				$model->save();
				$this->render('view',array(
					'model'=>$this->loadModel($id),
				));
			}
		}
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
		
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Messages;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		$senderId = Yii::app()->user->id;
		// print_r($senderId);exit;
		if(isset($_POST['Messages']))
		{
			$model->attributes=$_POST['Messages'];

			$model->mfrom = $senderId;
			$model->userid = $_POST['userid'];
			$model->event_id = $_POST['event_id'];
			$model->mto = $_POST['mto'];
			$model->mtype = 'forward'; // When message sent to another side
			
			if($model->save()){
				Yii::app()->user->setFlash('saved', 'Message has been sent!');
				$this->redirect(array('all','id'=>$model->ID));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Messages']))
		{
			$model->attributes=$_POST['Messages'];
			$model->mtype = 'forward'; // When message sent to another side
			$model->mread = 0;
			if($model->save()){
				$this->redirect(array('index','id'=>$model->ID));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Messages');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Messages('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Messages']))
			$model->attributes=$_GET['Messages'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Messages the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Messages::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Messages $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='messages-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
