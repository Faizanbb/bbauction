$(document).ready(function(){
    
        $('.datetimepicker').datetimepicker({
                format:'Y-m-d H:m:s',
                show:true
        });
   
    
       
        
        $('.wysiwygEditor').each(function(){
               console.log($(this).attr('id'));
               ClassicEditor
               .create( document.querySelector( "#"+$(this).attr('id') ) )
               .catch( error => {
                   console.error( error );
               } );    
           })
        
       $('input[type=radio][name="Events[event_type]"]').change(function() {
            if (this.value == 1) {
               $('#auction').hide();
               $('#rfq').show();
            }
            else if (this.value == 2) {
               $('#auction').show();
               $('#rfq').hide();
            }
        }); 
        
//        check if event type is selected auto show its body
        if($('#Events_questionnaire').is(':checked',true)){
              $('#questionaire_fields').show();
             
        }else
            $('#questionaire_fields').hide();
        if($('#Events_event_type_0').is(':checked',true)){
              $('#auction').hide();
               $('#rfq').show();
        }else if($('#Events_event_type_1').is(':checked',true)){
                $('#auction').show();
               $('#rfq').hide();
        }
        changeDynamicClosePeriod($('#Auctions_dynamic_close_period').val());
        auctionWeighting($('#Auctions_weighting').val());
        changerfqWeighting($('#Rfq_weighting').val());
        
        // component lots
        $('input[type=radio][name="LotsComponent[field_type]"]').change(function() {
           if($(this).val()=='0'){
              $('.price-sec').show();
              $('.pick-list').hide();
               
           }else if($(this).val()=='4'){
               $('.pick-list').show();
               $('.price-sec').hide();
           }else{
              $('.price-sec').hide();
              $('.pick-list').hide();
               
               
           }
        });
        $('input[type=radio][name="LotsComponent[uom]"]').change(function() {
           if($(this).val()=='0'){
              $('.default-mou').hide();
              $('.set-mou').hide();
           }else if($(this).val()=='1'){
              $('.default-mou').show();
              $('.set-mou').hide();
           }else if($(this).val()=='2'){
              $('.set-mou').show();
               $('.default-mou').hide();
           }
        });
        
        
    });
    function addTextEditor(el){
            ClassicEditor
            .create( document.querySelector( "#"+el) )
            .catch( error => {
                console.error( error );
            } );    
        
    }
    function changeDynamicClosePeriod(value){
        if(value!=0){
            $('.applies_to').show();
        }else
            $('.applies_to').hide();
    }
    function auctionWeighting(value){
        if(value!=0){
            $('.auction-weighting').show();
        }else
            $('.auction-weighting').hide();
    }
    function changerfqWeighting(value){
        if(value!=0){
            $('.rfq-weighting').show();
        }else
            $('.rfq-weighting').hide();
    }
    function changeAddQuestionaire(id){
        if($('#'+id).is(':checked',true)){
            $('#questionaire_fields').show();
            console.log('in');
        }else{
            $('#questionaire_fields').hide();
            console.log('out');
        }
    
    }
    function changeQuestionaireWithScoring(id,index){
        console.log($('#'+id).is(':checked',true));
        if($('#'+id).is(':checked',true)){
            $('#question_with_weight_'+index).show();
        }else
            $('#question_with_weight_'+index).hide();
    }
    var quesCount = 2;
    function addNewQuestionaier(id){
        var quesno = quesCount+1;
        var numItems = $('.Questionaire').length
        var qucount = numItems+1;
        var data = '<div class="Questionaire">\n\
<h3>Questionaire # '+qucount+'</h3>\n\
<input type="hidden" name="Questionaire['+quesCount+'][ID]"/><div class="form-group"><label >What do you want to call it ?</label><input  class="form-control questionaire-title" onchange="changeQuestionairetitle()"  name="Questionaire['+quesCount+'][title]"><small  class="form-text text-muted">(e.g. RFI, RFP, RFQ, PQQ, Supplier Self certification, Survey)</small></div>'+
                '<div class="form-group"><label >Deadline</label><input  class="form-control datetimepicker" name="Questionaire['+quesCount+'][deadline]"></div>'+
'<div class="form-check"><input class="form-check-input" type="checkbox" name="Questionaire['+quesCount+'][with_scoring]" id="questionaire_with_scoring_'+quesCount+'" onchange="changeQuestionaireWithScoring(this.id,'+quesCount+')" ><label class="form-check-label" >  Do you want it to have scoring</label></div>'+
'<div class="form-group nodisplay " id="question_with_weight_'+quesCount+'"><label >Do you want it to have weighting</label><select class="form-control" name="Questionaire['+quesCount+'][with_weight]" id="questionaire_with_weight_'+quesCount+'"  ><option value="'+quesCount+'">No</option><option value="1">Per Section</option><option value="2">Per Question</option><option value="3">Both Per Section & Per Question</option></select></div></div>';
        $('#extra_questionaire').append (data)
        quesCount++;
        $('#'+id).prop('checked', false); 
        $('.datetimepicker').datetimepicker({
                format:'Y-m-d H:m:s',
                show:true
        });
    }
    
    function changeQuestionairetitle(){

        $('#Rfq_q_id').html('');
            $('#Auctions_q_id').html('');
        var values = $("input[name^=Questionaire]").each(function(){
            var thda =  $(this)[0]
            if($(thda).hasClass('questionaire-title')){
                $('#Rfq_q_id').append('<option>'+$(thda).val()+'</option>');
               $('#Auctions_q_id').append('<option>'+$(thda).val()+'</option>');
            }
        });
      
    }
    
    
//    Section => Questions => Question Types
    
    function addNewQuestion(el){
       
        var secIndex = $(el).closest('.section').data('sec');
        var questIndex = $(el).siblings('.questions:last').data('secquest');
        console.log($(el).siblings('.questions:last'));
        var quesWeighting='';
        if($('#Questionaier_weighting').val()==2||$('#Questionaier_weighting').val()==3){
             quesWeighting = " <div class='form-group'> <label>Weighting</label> <input type='text' class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][weighting]' /> </div> ";
        }else{quesWeighting='';}
        questIndex = questIndex+1;
        var nexQuestionInex = questIndex+1;
        var element = "<div class='questions' data-secquest='"+questIndex+"'> <div class='single-question'> <h1>Question <span>"+nexQuestionInex+"</span></h1> <div><button class='btn btn-danger' onclick='deleteQuestion(this)'>Delete</button></div> <div class='form-group'> <label>Description</label> <textarea id='Sections_"+secIndex+"_Questions_"+questIndex+"_descrption' class='form-control wysiwygEditor' name='Sections["+secIndex+"][Questions]["+questIndex+"][descrption]'></textarea> </div> <div class='form-group'> <label>Scoring Criteria</label> <textarea id='Sections_"+secIndex+"_Questions_"+questIndex+"_qcriteria' class='form-control wysiwygEditor' name='Sections["+secIndex+"][Questions]["+questIndex+"][qcriteria]'></textarea> </div> <div class='form-group'> <label>Scoring</label> <input type='text' class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][qscoring]' /> </div>"+quesWeighting+"<div class='form-group'> <label> Question Type</label> <select class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][question_type]' onchange='addQuestionType(this);' > <option value=''>Please Select</option> <option value='yes_no'>Yes/No</option> <option value='pick_one_from_list'>Pick One From List</option> <option value='one_line_text'>One Line Text</option> <option value='paragraph_text'>Paragraph Text</option> <option value='document_upload'> Upload Document</option> <option value='date'>Date</option> <option value='table'>Table</option> </select> </div> <div class='question-type-container'> </div> </div> </div>";
        
        $(el).siblings('.questions:last').after(element);
        addTextEditor("Sections_"+secIndex+"_Questions_"+questIndex+"_descrption");
        addTextEditor("Sections_"+secIndex+"_Questions_"+questIndex+"_qcriteria");
    }
   
    function addNewSection(el){
        var secIndex = $(el).siblings('.section:last').data('sec');
        var newSecIndex = secIndex+1;
        var nextques = newSecIndex+1;
        var secWeightingElement='';
        var quesWeightingElement='';
        
        if($('#Questionaier_weighting').val()==3){
         secWeightingElement = " <div class='form-group'> <label>weighting</label> <input type='text' name='Sections["+newSecIndex+"][weighting]' value='100' class='form-control' /> </div>";
         quesWeightingElement = "<div class='form-group'> <label>Weighting</label> <input type='text' class='form-control' name='Sections["+newSecIndex+"][Questions][0][weighting]' value='100' /> </div>";
        }else if($('#Questionaier_weighting').val()==1){
         secWeightingElement = " <div class='form-group'> <label>weighting</label> <input type='text' name='Sections["+newSecIndex+"][weighting]' value='100' class='form-control' /> </div>";
         quesWeightingElement = "";
        }else if($('#Questionaier_weighting').val()==2){
         secWeightingElement = "";
         quesWeightingElement = "<div class='form-group'> <label>Weighting</label> <input type='text' class='form-control' name='Sections["+newSecIndex+"][Questions][0][weighting]' value='100' /> </div>";
        }else{secWeightingElement = '';var quesWeightingElement='';}
        var element = "<div class='section' data-sec='"+newSecIndex+"'> <h2>Section # "+nextques+"</h2><div><button class='btn btn-danger' onclick='deleteSection(this)'>Delete</button></div> <div class='form-group'> <label>Title</label> <input type='text' name='Sections["+newSecIndex+"][name]' class='form-control' /> </div> <div class='form-group'> <label>Description</label> <textarea type='text' name='Sections["+newSecIndex+"][description]' id='Sections_"+newSecIndex+"_description' class='form-control wysiwygEditor' ></textarea> </div>"+secWeightingElement+"<div class='section-question'> <div class='questions' data-secquest='0'> <div class='single-question'> <h1>Question1</h1> <div class='form-group'> <label>Description</label> <textarea class='form-control wysiwygEditor' id='Sections_"+newSecIndex+"_Questions_0_description' name='Sections["+newSecIndex+"][Questions][0][descrption]'></textarea> </div> <div class='form-group'> <label>Scoring Criteria</label> <textarea class='form-control wysiwygEditor' id='Sections_"+newSecIndex+"_Questions_0_qcriteria' name='Sections["+newSecIndex+"][Questions][0][qcriteria]'></textarea> </div> <div class='form-group'> <label>Scoring</label> <input type='text' class='form-control' name='Sections["+newSecIndex+"][Questions][0][qscoring]' /> </div> "+quesWeightingElement+" <div class='form-group'> <label> Question Type</label> <select class='form-control' name='Sections["+newSecIndex+"][Questions][0][question_type]' onchange='addQuestionType(this);' > <option value=''>Please Select</option> <option value='yes_no'>Yes/No</option> <option value='pick_one_from_list'>Pick One From List</option> <option value='one_line_text'>One Line Text</option> <option value='paragraph_text'>Paragraph Text</option> <option value='document_upload'> Upload Document</option> <option value='date'>Date</option> <option value='table'>Table</option> </select> </div> <div class='form-group question-type-container'> </div> </div> </div> <div class='btn btn-primary' onclick='addNewQuestion(this)'>add question</div> </div> </div>";
        $(el).siblings('.section:last').after(element);
        addTextEditor("Sections_"+newSecIndex+"_description");
        addTextEditor("Sections_"+newSecIndex+"_Questions_0_description");
        addTextEditor("Sections_"+newSecIndex+"_Questions_0_qcriteria");
    }
   
    function addQuestionType(el){
        var secIndex = $(el).closest('.section').data('sec');
        var questIndex = $(el).closest('.questions').data('secquest');
        var subquesIndex=0;
        if($(el).val()=='yes_no'){
        var element = "<div class='yes-no-question'> <table class='table'> <thead> <tr> <td></td> <td>Scoring</td> <td>Fail</td> </tr> </thead> <tbody> <tr> <td><input type='text'  class='form-control'  value='Yes' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][0][option]' /></td> <td><input type='text' class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][0][scoring]' /></td> <td> <select value='Yes' class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][0][fail]' >  <option>No</option><option>Yes</option> </select> </td> </tr> <tr> <td><input type='text'  class='form-control'  value='No' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][1][option]' /></td> <td><input type='text' class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][1][scoring]' /></td> <td> <select value='Yes' class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][1][fail]' >  <option>No</option><option>Yes</option> </select> </td> </tr> </tbody> </table> </div>";
    }else if($(el).val()=='pick_one_from_list'){
        var element = "<div class='pic-one-question'> <table class='table'> <thead> <tr> <td></td> <td>Scoring</td> <td>Fail</td> </tr> </thead> <tbody> <tr data-picone='"+subquesIndex+"'> <td><input type='text' class='form-control' placeholder='Options' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers]["+subquesIndex+"][option]' /></td> <td><input type='text' class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers]["+subquesIndex+"][scoring]' /></td> <td> <select class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers]["+subquesIndex+"][fail]' >  <option>No</option><option>Yes</option> </select> </td> </tr> </tbody> </table> <div class='btn btn-default' onclick='addMultiQuestionField(this);'>Add Field</div> </div>";
    }else if($(el).val()=='one_line_text' || $(el).val()=='paragraph_text'){
        var element = "<div class='text-field-question'> <div class='form-group'><label>Maximum number of allow characters</label><input class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers]["+subquesIndex+"][num_of_allowed_txt]' /></div> </div>";
    }else if($(el).val()=='table'){
        var element = "<div class='table-question'><table class='table form-group'><thead><tr><td><lable>Row</label></td><td><input class='form-control q-t-rows' /></td><td><label>Cols</label></td><td><input class='form-control q-t-cols' /></td><td><div class='btn btn-success' onclick='addTableField(this)'>Add Table</div></td></tr></thead></table></div>";
    }else if($(el).val()=='table'){
        var element = "<div class='table-question'> <div class='row'><label class='col-sm-2'>Rows</label><div class='col-sm-3'><div class='form-group'><input class='form-control' id='q-t-cols' /></div></div><div class='col-sm-3'><div class='form-group'><input class='form-control' id='q-t-rows' /></div></div><div class='col-sm-2'><button class='btn btn-primay'>Add</button></div></div> </div>";
    }
    
    $(el).closest('.questions').find('.question-type-container').html(element);
    }
   
    function addMultiQuestionField(el){
        var secIndex = $(el).closest('.section').data('sec');
        var questIndex = $(el).closest('.questions').data('secquest');
        console.log();
        var subquesIndex = $(el).closest('.pic-one-question').find('tbody tr').last().data('picone');
        var subquesIndex = subquesIndex+1;
        console.log(subquesIndex);
        var element = "<tr data-picone='"+subquesIndex+"'> <td><input type='text' class='form-control' placeholder='Options' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers]["+subquesIndex+"][option]' /></td> <td><input type='text' class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers]["+subquesIndex+"][scoring]' /></td> <td> <select class='form-control' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers]["+subquesIndex+"][fail]' >  <option>No</option><option>Yes</option> </select> </td> <td> <div class='btn btn-danger' onclick='removeTrElement(this)'>delete</div></td></tr>";
         $(el).closest('.pic-one-question').find('tbody tr:last').after(element);
        
    }
    
    
    function addTableField(el){
        var secIndex = $(el).closest('.section').data('sec');
        var questIndex = $(el).closest('.questions').data('secquest');
        
        var rows =  $(el).closest('table').find('.q-t-rows').val();
        var cols =  $(el).closest('table').find('.q-t-cols').val();
        var elementhead= '';
        var elementbody= '<tbody>';
        var elementrow= '';
        console.log('rows = '+rows+" cols = "+cols)
        for(var i = 1;i<=rows;i++){
            elementrow = '<tr>'
            for(var j=1;j<=cols;j++){
                if(i==1)
                elementrow = elementrow+ "<td> <input class='text-center noborder' placeholder='Col "+i+"' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][Row]["+i+"]["+j+"]' /></td>";
                else{
                    if(j==1)
                        elementrow = elementrow+ "<td> <input placeholder='Row "+j+"' class='text-center noborder' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][Row]["+i+"]["+j+"]'  /></td>";
                    else
                        elementrow = elementrow+ "<td> <input  placeholder='Answer' class='text-center not-allowed noborder' name='Sections["+secIndex+"][Questions]["+questIndex+"][QuestionAnswers][Row]["+i+"]["+j+"]' readonly  /></td>";
                }
                    
            }
           elementrow =elementrow + '<\tr>';
           if(i==1)
               elementhead = '<thead>'+elementrow+'</thead>';
           else
               elementbody = elementbody+ elementrow; 
           
          

       }
        elementbody = elementbody+'</tbody>';
        var element = elementhead+elementbody;
        var nel = $(el).closest('table');
        $(el).closest('table').html(element);
        $(nel).addClass('table-bordered');
        
       
    }
    
    function validateSections(){
        var status = true;
        var message = '';
        var tsecweighting = 0;
        $('.section').each(function(){
            var secId =  $(this).data('sec');
            secweighting = 0;
            var secweighting = $('[name^="Sections['+secId+'][weighting]"]').val();
            tsecweighting  = parseFloat(secweighting)+parseFloat(tsecweighting);
            var tqweighting = 0;
            var secqweighting = 0;
            $(this).find('.questions').each(function(){
              var qid = $(this).data('secquest');
               secqweighting = parseFloat(secqweighting) + parseFloat($('[name^="Sections['+secId+'][Questions]['+qid+'][weighting]"]').val());
               console.log(secId+'-'+secweighting+'-'+qid+'-'+secqweighting);
              tqweighting = parseFloat(tqweighting)+ parseFloat($('[name^="Sections['+secId+'][Questions]['+qid+'][weighting]"]').val());
           });
            if($('#Questionaier_weighting').val()==3){
                if(secqweighting!=100){
                   message = 'Sec Questions weighting should be 100 Sec: '+secId+' <br>';
                   status = false;
                }
            }
            if($('#Questionaier_weighting').val()==2){
                if(tqweighting!=100){
                   message = 'All Questions weighting should add up to 100 <br>';
                   status = false;
                }
            }
            if($('#Questionaier_weighting').val()==1 || $('#Questionaier_weighting').val()==3){
                if(tsecweighting!=100){
                    console.log(tsecweighting);
                   message = 'Total Sections weighting should be 100; <br>';
                   status = false;
                }
            }
           
           if(!status){
               console.log('123');
               $('.alert-danger').show();
               $('.validation-error').html(message);
           }else{
               $('.alert-danger').hide();
           }
           
        })
        
    }
    
    function removeTrElement(el){
        console.log(el);
        $(el).closest('tr').remove();
    }
    
    function deleteQuestion(el){
         $(el).closest('.questions').remove();
    }
    
    function deleteSection(el){
         $(el).closest('.section').remove();
    }
    
    
    // advanced lot
    
    function  addPickListField(el){
        var subquesIndex = $(el).closest('.pick-list').find('tbody tr').last().data('picklist');
        console.log(subquesIndex);
        var subquesIndex = subquesIndex+1;
        
        var element = "<tr data-picklist='"+subquesIndex+"'> <td><input type='text' class='form-control' placeholder='Option' name='LotsComponent[pick_list]["+subquesIndex+"][option]' /></td> <td><input type='text' class='form-control' name='LotsComponent[pick_list]["+subquesIndex+"][value]' placeholder='Value' /></td> <td> <div class='btn btn-danger' onclick='removeTrElement(this)'>delete</div></td></tr>";
         $(el).closest('.pick-list').find('tbody tr:last').after(element);
        
    }